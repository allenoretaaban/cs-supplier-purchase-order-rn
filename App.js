import React from "react";
import Setup from "./src/boot/setup";
import 'react-native-gesture-handler';
import { MenuProvider } from 'react-native-popup-menu';

export default class App extends React.Component {
  render() {
	return <MenuProvider><Setup /></MenuProvider>;
  }
}
