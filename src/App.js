import React from 'react';
import { Root } from "native-base";
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import 'react-native-gesture-handler';

import Home from "./screens/home/";
import Login from "./screens/login/";
import LoadingScreen from "./screens/loader/";
import SideBar from "./screens/sidebar";

import Database from './db/db';
const db = new Database();
db.initDB();

const Drawer = createDrawerNavigator(
  {
    LoadingScreen: { screen:LoadingScreen },
    Home: { screen:Home },
    Login: { screen:Login }
  },
  {
    initialRouteName: 'LoadingScreen',
    edgeWidth: 0,
    contentOptions: {
      activeTintColor: '#e91e63'
    },
    contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = createStackNavigator(
  {
    Drawer: { screen: Drawer },
    LoadingScreen: { screen: LoadingScreen }
  },
  {
    // initialRouteName: 'LoadingScreen',
    defaultNavigationOptions: {
      headerShown: false
    }
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
