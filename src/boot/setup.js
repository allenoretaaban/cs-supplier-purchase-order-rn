import React, { Component } from "react";
import { StyleProvider } from "native-base";
import getTheme from "../theme/components";
import material from "../theme/variables/material";
import App from "../App";
import { LogBox } from "react-native"; 

export default class Setup extends Component {
  render() {
    return (      
      <StyleProvider style={getTheme(material)}>
        <App/>
      </StyleProvider>
    );
  }
}

LogBox.ignoreAllLogs();
