const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  logoContainer: {
    position: "absolute",
    flex: 1,
    marginTop: deviceHeight / 5,
    marginBottom: 0
  },
  logoTitle: {
    fontWeight:'bold',
    fontSize:60,
    alignSelf:'center',
    marginTop:150,
    height:null
  },
  logo: {
    left: Platform.OS === "android" ? 100 : 100,
    width: 280,
    height: 100
  },
  overlay: {
    flex: 1,
    backgroundColor:'rgba(0, 0, 0, 0.5)'
  }
};
