import React, { Component, PureComponent } from "react";
import { ImageBackground, Dimensions, StatusBar, Keyboard, TouchableWithoutFeedback, FlatList, VirtualizedList, TouchableHighlight,
  TouchableOpacity, SafeAreaView, KeyboardAvoidingView, Alert, TextInput, View, Text, Item, Input, ActivityIndicator, Pressable,
  TouchableHighlightBase, ScrollView, TouchableNativeFeedback } from "react-native";
import { Button, Icon } from "native-base";
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import Toast, { DURATION } from "react-native-easy-toast";
import AsyncStorage from '@react-native-async-storage/async-storage';

import cons from "./../../constants/csvars";
//import vars from "./../../theme/variables/material";
//import SlidingView from 'rn-sliding-view';
//import { Col, Row, Grid } from "react-native-easy-grid";
import rest from "./../../utility/requests";
import helpers from "./../../utility/helpers";
import Modal from "react-native-modal";
import styles from "./styles.table";

import Database from "./../../db/db";
import { set } from "react-native-reanimated";
const db = new Database();

var loaderCtr = 0;
var itemCtr = 0;
var offSet = 0; var strPageSize = 7;
var getSuppLoadingFlag = 0;
var isSingleSearch = 0;
const { width,height } = Dimensions.get("window"); // not working on vars
const widths = 340;

class Home extends PureComponent {

  constructor(props) {
    console.log('constructor Home');
    super(props);    
    // this._clickItem = this._clickItem.bind(this);
    this.state = { sData:[], searchText:'', isLoading:false, ldrLft:width, keyPadLoc:width, suppBlock:-widths, rowBg:[], orderVals:[], tigger:[],
      selected:null  }
    this.tblItemsContainer = []; this.tblItemsContainerDummy = [];
    this.tblActualItems = [];
    this.sIds = [];
    this.sNames = [];
    this.itemPointer = null;
    this.idEmployee = "";
    this.refUrl = "";
    this.rowBg = [...[]];

    //db.deleteRset(cons.TBL.POREF, "", null).then((data) => { });
    db.getRset(cons.TBL.POREF, "", "", []).then((data) => { console.warn(data); }).catch((err) => { console.error(err); });
    this._initItems();
  }

  componentDidMount() {
    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      console.log('componentDidMount Home');

      this._initItems();
      this._getAuth();
      this._getConnType();

    });
  }

  _initItems = () => {
    offSet=0;isSingleSearch=0;this.getDbSuppliersPage('');
  }

  _getAuth = async() => {
    try {
      this.idEmployee = await AsyncStorage.getItem('csrEmpId');
    } catch (error) { }
  }

  _getConnType = async() => {
    try {
      let connType = await AsyncStorage.getItem('csrEmpId');
      console.log(connType);
    } catch (error) { }
  }

  _hideAll = () => {
    Keyboard.dismiss(); this.setState({suppBlock:-widths}); 
  }

  _showSuppliers = () => {
    this.setState({suppBlock:0,keyPadLoc:width}); 
    this.getDbSuppliers('');
  }

  _onSearch = (event, flag) => {
    this.getDbSuppliers(event.nativeEvent.text.toString());
  }

  _onMenuSelect = (val) => {
    switch(val) {
      case 1:
        Alert.alert( 'Logout', '\nAre you sure you want to log-out?',
          [
            { text:'Cancel', onPress:() => { this.setState({ isLoading:false }); }, style:'cancel' },
            { text:'OK', onPress:() => {
              this.setState({ isLoading:true });
              setTimeout(() => {
                // resets
                this.sIds = [];
                this.sNames = [];
                this.tblItemsContainer = [];
                this.tblActualItems = [];
                loaderCtr = 0;
                itemCtr = 0;

                this.setState({ isLoading:false });
                this.props.navigation.navigate('Login');
              }, 1000);
            }
            },
          ],
          {cancelable: false},
        );
        break;
      default:
        Alert.alert( 'About', '\nCreativeScript\nSupplier Purchase Order\nCopyright \u00A9 2020',
          [
            { text:'OK', onPress:() => { }, style:'cancel' },
          ],
          {cancelable: false},
        );
        break;
    }

  }

  _clickItem = async(id) => {
    this._hideAll(); 

    this.setState({keyPadLoc:100});
    setTimeout(() => {
      this.setState({rowBg:[...['']]});
      let aBg = [...this.state.rowBg];
      aBg[id] = '#222';
      this.setState({rowBg:aBg});
    }, 0);

    this.itemPointer = id; 
    return false;
  }

  _handleInputData = (idx, value) => {
    let nArr = [...this.state.orderVals]; 
    if (typeof nArr[idx] === 'undefined') nArr[idx] = '';
    setTimeout(() => {
      switch (value) {
        case "E":
          var strQty = nArr[idx];
          if (typeof strQty === 'undefined') { this.refs.eToast.show('INVALID REFERENCE!\nPlease select an item.'); return; }
          if (strQty.toString().trim() == '') { this.refs.eToast.show('INVALID QUANTITY!\nPlease input quantity.'); return; } 
          this.setState({keyPadLoc:width});
          this.restPostPO(idx);
          break;
        case "X":
          this.setState({keyPadLoc:width});
          break;
        case "C":
          nArr[idx] = "";  
          this.setState({orderVals:nArr});
          break;
        default:      
          let sVal = ''+nArr[idx];
          if (sVal.length > 0) {
            switch(value) {
              case ".": 
                sVal = sVal.indexOf('.') !== -1 ? sVal : sVal+value ;
                break;
              default: sVal = sVal == "0" ? value : sVal+value; break;                                                       
            }
          } else {   
            sVal = value == "." ? "0." : value ;
          }
          nArr[idx] = sVal;
          this.setState({orderVals:nArr});
          break;
      }
    }, 0);
  }

  _getItem = (data, index) => {
    let dRs = null;
    if (typeof data === 'undefined') {  
      return {}
    } else { 
      dRs=data.length>0?data[index]:[]; 
      return this._getItemModel(dRs);
    }
  }

  _isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };

  _showNextBatch = ()  => {
    if (isSingleSearch == 1) return;
    if (getSuppLoadingFlag == 0) {
      offSet = offSet + strPageSize;
      console.log('next '+ offSet);
      this.getDbSuppliersPage('');
    }
  };

  _saveAll = () => {
    Keyboard.dismiss();
    this.setState({keyPadLoc:width,suppBlock:-widths});

    Alert.alert( 'Save All PO Requests', '\nAre you sure you want to save all PO request?',
      [
        { text:'Cancel', onPress:() => { this.setState({ isLoading:false }); }, style:'cancel' },
        { text:'OK', onPress:() => {
          this.refs.iToast.show('Processsing Request.... Please wait.', DURATION.LONG);
          this.setState({ isLoading:true });
          setTimeout(() => {
            this.refs.sToast.show('Request Success...!', DURATION.LONG);
            this.setState({ isLoading:false });
            this.getDbSuppliers('',0);
          }, 2000);
        }
        },
      ],
      {cancelable: false},
    );
  }

  /*_renderRowItem = (item) => {
    return <TouchableHighlight style={{flex:1,marginStart:5,marginEnd:4}} key={item.rid} onPress={() => this._clickItem(item.rid)} >
      <View style={{flexDirection:'row'}}>
        <View style={styles.tblCPre}>
          <Text style={styles.txtC}>{item.itemname}</Text>
        </View>
        <View style={styles.tblCc}>
          <Text style={styles.txtC}>{item.unit}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.B1}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.B2}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.B3}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.cold_two}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{helpers.nWithCommas(
            helpers.formatStrToInt(item.B1) + helpers.formatStrToInt(item.B2) + helpers.formatStrToInt(item.B3) + helpers.formatStrToInt(item.cold_two)
          )}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.used}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.pending}</Text>
        </View>
        <View style={{ borderColor:'#405323', borderRightWidth:1, borderBottomWidth:1, minHeight:38, alignItems:'center', justifyContent:'center', 
          alignItems:'center', width:80, paddingStart:7, paddingEnd:7, backgroundColor:this.state.rowBg[item.rid] }}>
          <View style={{flex:1,margin:2,width:'100%',justifyContent:'center',alignItems:'center'}}>
            <TextInput style={{color:"gold",fontSize:16,fontFamily:'Lato-Regular'}} placeholder={item.ordero}  value={item.order} 
              editable={false} placeholderTextColor='gray' />
          </View>
        </View>
      </View>
    </TouchableHighlight>
  }*/

  render() {

    const renderItemTip = ({item}) => {
      try {
        switch(item.type) {
          case 'end':
            return <View key={item.rid} style={{flex:1,paddingVertical:40}} hidden><Text style={{'color':'gold',flex:1,textAlign:'center'}}>NOTHING FOLLOWS...</Text></View>;
          case 'empty':
            return <TouchableOpacity onPress={() => { this._showNextBatch(); }}><View key={item.rid} style={{flex:1,paddingVertical:40}} hidden><Text style={{'color':'gold',flex:1,textAlign:'center'}}>NEXT SUPPLIERS...</Text></View></TouchableOpacity>;
          case 'loader':
            return <View key={item.rid} style={{flex:1,paddingVertical:40}} hidden><ActivityIndicator style={{flex:1}} color="#fad2de" /></View>;
          case 'item':
            //return this._renderRowItem(item);
            return <TouchableOpacity style={{flex:1,backgroundColor:this.state.rowBg[item.rid],marginStart:5,marginEnd:4,marginTop:0,marginBottom:0,paddingTop:0,paddingToBottom:0}} key={item.rid} onPress={() => { this._clickItem(item.rid); }}>
              {/* <RowItem item={item} key={item.rid} onpressitem={() => this._clickItem(item.rid)} rowbg={this.state.rowBg} orderVals={this.state.orderVals} /> */}
              <RowItem item={item} key={item.rid} orderVals={this.state.orderVals} trigger={this.state.trigger} />
            </TouchableOpacity>;
          case 'header':
            return <TouchableOpacity key={item.rid} onPress={() => { this.setState({keyPadLoc:width}); this._hideAll(); }}><RowHeader /></TouchableOpacity>;
          default:
            return <TouchableOpacity key={item.rid} onPress={() => { this.setState({keyPadLoc:width}); this._hideAll(); }}><RowSupplier item={item} /></TouchableOpacity>; 
            break;
        }
      } catch(e) {
        console.error(e);
      }
    }

    let xflag = 0;

    return (
      <View style={{backgroundColor:'black',flex:1}}>
        <KeyboardAvoidingView style={{ flex:1 }} enabled={true} behavior={Platform.OS==='ios'?'padding':null} >
          <StatusBar barStyle="light-content" />
          <View style={{height:40,alignItems:'center',justifyContent:'center',backgroundColor:'rgba(64,83,35, 0.3)'}} >
            <Text style={{color:'lightgray',fontSize:18,fontFamily:'Lato-Regular'}}>Supplier Purchase Order / Stock Running Balance</Text>
          </View>
          <TouchableHighlight onPress={() => { this.setState({keyPadLoc:width}); this._hideAll(); }}>
            <View style={{height:40,justifyContent:'flex-start',alignItems:'center',backgroundColor:'rgba(64,83,35, 0.3)',marginTop:2,flexDirection:'row'}} >
              <Text style={{color:'lightgray',fontSize:14,fontFamily:'Lato-Regular',marginStart:15}}>As of {helpers.getCurrentDateTimeDisplay()}</Text>
              <View style={{flex:1,justifyContent:'flex-end',flexDirection:'row',alignItems:'center'}}>
                <Button onPress={() => this._saveAll()} style={{height:31,paddingEnd:10,marginEnd:4,paddingTop:5}} >
                  <Icon name='ios-save' style={{fontSize:20,color:'#4e4e4e'}} /><Text style={{fontSize:15,color:'#4e4e4e',marginTop:2}}>Save All</Text>
                </Button>
                <Button onPress={() => { this._initItems(); }} style={{height:31,paddingEnd:10,marginEnd:4,paddingTop:5}} >
                  <Icon name='ios-refresh' style={{fontSize:20,color:'#4e4e4e'}} /><Text style={{fontSize:15,color:'#4e4e4e',marginTop:2}}>Reload</Text>
                </Button>
                <Button onPress={() => this._showSuppliers()} style={{height:31,paddingEnd:10,marginEnd:4,paddingTop:5}} >
                  <Icon name='ios-search' style={{fontSize:20,color:'#4e4e4e'}} /><Text style={{fontSize:15,color:'#4e4e4e',marginTop:2}}>Search</Text>
                </Button>
                <Menu onSelect={value => { this._onMenuSelect(value); }} >
                  <MenuTrigger style={cons.STYLE_MTRIG}><Icon name="menu" style={{color:'#fad2de'}}/></MenuTrigger>
                  <MenuOptions style={{backgroundColor:'#1c2110'}}>
                    <MenuOption value={0} style={cons.STYLE_MOPT} key={1}>
                      <Icon name="information-circle-outline" style={cons.STYLE_MOPT_I}/>
                      <Text style={cons.STYLE_MOPT_T}>About</Text>
                    </MenuOption>
                    <MenuOption value={1} style={cons.STYLE_MOPT} key={2}>
                      <Icon name="lock-closed-outline" style={cons.STYLE_MOPT_I}/>
                      <Text style={cons.STYLE_MOPT_T}>Log-Out</Text>
                    </MenuOption>
                  </MenuOptions>
                </Menu>
              </View>
            </View>
          </TouchableHighlight>

          <SafeAreaView style={{flex:1}}>
            <ImageBackground style={cons.CNTR_STYL}>
              <TouchableWithoutFeedback style={{flex:1}} onPress={() => { this._hideAll(); }}>
                <View style={cons.OVRLYA}>

                  <View style={{flex:1}} > 
                    <View style={{flex:1,marginBottom:0,backgroundColor:"transparent",paddingStart:0,paddingTop:0,paddingEnd:0}}>    

                      <TouchableWithoutFeedback style={{flex:1}} onPress={() => { this._hideAll(); }}>  
                        <FlatList
                          scrollEnabled={true}
                          onEndReachedThreshold={-0.2}
                          onEndReached={(val) => { 
                            console.log('next '+ offSet);
                            /*if (isSingleSearch == 1) return;
                            if (getSuppLoadingFlag == 0) {
                              offSet = offSet + strPageSize;
                              console.log('next '+ offSet);
                              this.getDbSuppliersPage('');
                            }*/
                          }}
                          refreshing={false}
                          onRefresh={() => { 
                            if (isSingleSearch == 1) return;
                            if (getSuppLoadingFlag == 0) {
                              offSet = offSet == 0 ? 0 : offSet - strPageSize;
                              console.log('prev '+offSet);
                              this.getDbSuppliersPage('');
                            }
                          }}
                          bounces={true}
                          keyboardShouldPersistTaps={'handled'}
                          shouldComponentUpdate={true}
                          legacyImplementation={true}
                          removeClippedSubviews={true}
                          maxToRenderPerBatch={20}
                          updateCellsBatchingPeriod={50}
                          initialNumToRender={25}
                          useNativeDriver={true}
                          windowSize={41}
                          style={{marginTop:0,flex:1}}
                          data={this.tblItemsContainer}
                          extraData={this.state.orderVals}
                          ItemSeparatorComponent = {this.FlatListItemSeparator}
                          renderItem={renderItemTip}
                          keyExtractor={(item) => item.rid.toString()}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <View style={{flex:1,position:"absolute",backgroundColor:"rgba(0,0,0,0.7)",left:this.state.ldrLft,height:"100%"}} hidden>
                        <ActivityIndicator style={{flex:1,width:width}} hide/>
                    </View>
                  </View>

                </View>
              </TouchableWithoutFeedback>
            </ImageBackground>   
          </SafeAreaView>

          <Toast ref="eToast" style={{backgroundColor:'#d9534f',paddingHorizontal:20,paddingVertical:15}} 
            textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
          <Toast ref="iToast" style={{backgroundColor:'#62B1F6',paddingHorizontal:20,paddingVertical:15}} 
            textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
          <Toast ref="sToast" style={{backgroundColor:'#5cb85c',paddingHorizontal:20,paddingVertical:15}} 
            textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
          <Toast ref="wToast" style={{backgroundColor:'#f0ad4e',paddingHorizontal:20,paddingVertical:15}} 
            textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
        </KeyboardAvoidingView>  

        <TouchableWithoutFeedback>
          <View style={{position:'absolute',bottom:0,end:this.state.keyPadLoc,paddingTop:5,paddingStart:5,flexDirection:'row'}}>
            <View>
              <View style={{flexDirection:'row',flex:0}}>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"7"); }} >                  
                  <Text style={styles.calBtnTxt}>7</Text>
                </Button>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"8"); }} >                 
                  <Text style={styles.calBtnTxt}>8</Text>
                </Button>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"9"); }} >                 
                  <Text style={styles.calBtnTxt}>9</Text>
                </Button>
              </View>
              <View style={{flexDirection:'row',flex:0}}>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"4"); }} >                  
                  <Text style={styles.calBtnTxt}>4</Text>
                </Button>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"5"); }} >                 
                  <Text style={styles.calBtnTxt}>5</Text>
                </Button>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"6"); }} >                 
                  <Text style={styles.calBtnTxt}>6</Text>
                </Button>
              </View>
              <View style={{flexDirection:'row',flex:0}}>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"1"); }} >                  
                  <Text style={styles.calBtnTxt}>1</Text>
                </Button>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"2"); }} >                 
                  <Text style={styles.calBtnTxt}>2</Text>
                </Button>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"3"); }} >                 
                  <Text style={styles.calBtnTxt}>3</Text>
                </Button>
              </View>
              <View style={{flexDirection:'row',flex:0}}>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"C"); }} >                  
                  <Text style={styles.calBtnTxtT}>CLR</Text>
                </Button>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"0"); }} >                 
                  <Text style={styles.calBtnTxt}>0</Text>
                </Button>
                <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"."); }} >                 
                  <Text style={styles.calBtnTxt}>.</Text>
                </Button>
              </View>
            </View>
            <View>
              <Button style={styles.calBtn} onPress={() => { this._handleInputData(this.itemPointer,"X"); }} >      
                <Icon name="close" style={{color:"orange",fontSize:30}}/>
              </Button>
              <Button style={styles.calBtnEnt} onPress={() => { this._handleInputData(this.itemPointer,"E"); }}>
                <Text style={styles.calBtnTxtT}>E{'\n'}N{'\n'}T{'\n'}E{'\n'}R</Text>
              </Button>
            </View>
          </View>
        </TouchableWithoutFeedback>

        <View style={{position:'absolute',top:41,end:this.state.suppBlock,height:height-41,width:widths,backgroundColor:'rgba(0,0,0,0.9)'}}>
          <View style={{flex:0,height:40,borderWidth:1,borderColor:'#fad2de',marginTop:5,marginHorizontal:5,borderRadius:20,paddingEnd:15,paddingStart:15,
            flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
            <TextInput style={{flex:1,color:'gold',fontSize:15,fontFamily:'Lato-Regular',height:40}} 
              onSubmitEditing={(event) => { getSuppLoadingFlag = 0; this._onSearch(event,1); }}
              value={this.state.searchText} 
              onChangeText={(value) => { this.setState({ searchText:value }) }}
              placeholder='Search here...' />
            <TouchableHighlight onPress={() => { getSuppLoadingFlag = 0; Keyboard.dismiss(); this.getDbSuppliers(this.state.searchText.trim(),1); }}>
              <Icon name='ios-search' style={{fontSize:22}} />
            </TouchableHighlight>
          </View>
          <FlatList
            useNativeDriver={true}
            scrollEnabled={true}
            onEndReachedThreshold={0.5}
            onEndReached={(val) => { if(val > 0) { console.log(val + ' then fetch new');} }}
            keyboardShouldPersistTaps={'handled'}
            shouldComponentUpdate={true}
            legacyImplementation={true}
            removeClippedSubviews={true}
            initialNumToRender={10}
            style={{marginTop:0,flex:1}}
            data={ this.state.sData }
            ItemSeparatorComponent = {this.FlatListItemSeparator}
            renderItem={({item, index}) => {
              return <TouchableHighlight style={{flex:1,marginStart:5,marginEnd:5,marginTop:5}} onPress={() => { 
                  getSuppLoadingFlag = 0;
                  Keyboard.dismiss(); 
                  offSet=0; isSingleSearch=1; this.getDbSuppliersPage(item.suppliername);
                }} key={index}>
                <RowSupplierR item={item} />
              </TouchableHighlight>;
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>    

        <Modal backdropColor='#000' backdropOpacity={0.4} isVisible={this.state.isLoading}>
          <View style={{ flex: 1 }}>
            <ActivityIndicator style={{ flex:1 }} />
          </View>
        </Modal>

      </View>    
    );
  }

  //////////////////////////////////////////// request ////////////////////////////////////////////

  getDbSuppliersPage = async(strSearch) => { 
    // resets
    this.itemPointer = 0;
    this.setState({rowBg:[...['']],keyPadLoc:width,orderVals:[...['']]});
    this.sIds = [];
    this.sNames = [];
    this.tblItemsContainer = []; this.tblItemsContainerDummy = [];
    this.tblActualItems = [];
    loaderCtr = 0;
    itemCtr = 0; 
    // blocking double call
    if (getSuppLoadingFlag == 1) return; getSuppLoadingFlag = 1;
    // query
    db.getRset(cons.TBL.SUPP, "WHERE suppliername LIKE ? LIMIT "+strPageSize+" OFFSET "+offSet, "", ['%'+strSearch+'%'])
    .then((jsn) => {
      for(var x=0;x<jsn.length;x++) {
        let rID = parseInt(jsn[x].supplierID);
        this.sIds.push(rID);
        this.sNames.push(jsn[x].suppliername);
      }
      this.setState({ isLoading:false });
      this.restItems(this.sIds[loaderCtr], this.sNames[loaderCtr], 0);
    }).catch((err) => { 
      console.error(err); 
      alert("Error on Request! Please inform DEVELOPER....");
    });
  }

  _getItemModel = (rsObj,type) => {
    let obj = {item_recid:'', supplierID:'', suppliername:type, itemname:'', unit:'', B1:'', B2:'', B3:'', cold_two:'', 
      Total:'', used:'', pending:'', order:'', ordero:'', rid:itemCtr, type:type, selected:0, saved:0 };
    if (typeof rsObj !== 'undefined') {
      let sOrder = rsObj.order == null ? '' : rsObj.order;
      let sB1 = rsObj.B1 == null ? '' : rsObj.B1;
      let sB2 = rsObj.B2 == null ? '' : rsObj.B2;
      let sB3 = rsObj.B3 == null ? '' : rsObj.B3;
      let scold_two = rsObj.cold_two == null ? '' : rsObj.cold_two;
      let sused = rsObj.used == null ? '' : rsObj.used;
      let spending = rsObj.pending == null ? '' : rsObj.pending;
      let sTotal = rsObj.Total == null ? '' : rsObj.Total;
      obj = {
        item_recid:rsObj.item_recid,
        supplierID:rsObj.supplierID,
        suppliername:rsObj.suppliername,
        itemname:rsObj.itemname,
        unit:rsObj.unit,
        B1:sB1, B2:sB2, B3:sB3,
        cold_two:scold_two,
        Total:sTotal,
        used:sused,
        pending:spending,
        order:'',
        ordero:sOrder.toString(),
        rid:itemCtr,
        type:type,
        selected:0,
        saved: 0
      };   
    }
    // save blanks
    /*let ovArr = [...this.state.orderVals];
    ovArr[itemCtr] = '';
    this.setState({orderVals:ovArr});*/
    return obj;
  } 

  restItems = async(rID, rName, flagBulk) => {  
    if (getSuppLoadingFlag == 0) return;

    this.tblItemsContainerDummy[itemCtr] = this._getItemModel(undefined,rName);
    this.tblItemsContainer[itemCtr] = this._getItemModel(undefined,rName); itemCtr++; // add supplier name

    this.tblItemsContainerDummy[itemCtr] = this._getItemModel(undefined,"loader");
    this.tblItemsContainer[itemCtr] = this._getItemModel(undefined,"loader");
    this.setState({trigger:[]});

    let url = rest.SERVER_BETA + 'api/spo/getSupplierItems?cn='+rest.CN+'&supplier_recid='+rID;
    // console.log(url);
    fetch(url, { headers:rest.HEADERS })
    .then((response) => { return response.json(); }).then((rxJson) => {
      console.log(loaderCtr+') '+rName+' ('+rxJson.length+')');
      if (rxJson.length > 0 ) {
        this.tblItemsContainerDummy[itemCtr] = this._getItemModel(undefined,"header");
        this.tblItemsContainer[itemCtr] = this._getItemModel(undefined,"header"); itemCtr++; // add header
      }

      for(var x=0;x<rxJson.length;x++) {
        let rsObj = rxJson[x];
        this.tblItemsContainerDummy[itemCtr] = this._getItemModel(rsObj,'item'); itemCtr++; // add item
        this.tblActualItems.push(this._getItemModel(rsObj,'item'));
      }

      if (loaderCtr < this.sIds.length-1) {
        console.log("next");
        this.tblItemsContainer = this.tblItemsContainerDummy;

        loaderCtr++;
        this.restItems(this.sIds[loaderCtr], this.sNames[loaderCtr], flagBulk);
      } else {
        console.log("done");
        this.setState({ isLoading:false });
        if (this.tblActualItems.length == 0) {
          this.refs.wToast.show('SOME ITEMS NOT FOUND!', DURATION.LONG);
        }

        if (isSingleSearch == 1) {
          console.log("isSingleSearch");
          this.tblItemsContainerDummy[itemCtr] = this._getItemModel(undefined,"end");
        } else {
          this.tblItemsContainerDummy[itemCtr] = this._getItemModel(undefined,"empty");
        }
        this.tblItemsContainer = this.tblItemsContainerDummy;
        this.setState({trigger:[]});
        getSuppLoadingFlag = 0;
        // console.log(this.tblItemsContainer);
      }
    }).catch((error) => {
      console.error(error);
      alert("Error on Request! Please inform DEVELOPER....");
    });
  }
  
  restPostPO = (id) => {  
    this.setState({ ldrLft:0 });
    let objRs = this.tblItemsContainer[id]; console.log(objRs);
    let sID = objRs.supplierID;
    let poID = helpers.getPODate();
    db.getRset(cons.TBL.POREF, "WHERE supplierID = ?", "", [''+sID]
    ).then((data) => {
      if (data.length > 0 ) { poID = data[0].porecid; }
      let url = rest.SERVER_BETA + 'api/spo/postPurchaseOrder?cn='+rest.CN
        +'&porecid='+poID
        +'&podetails_recid='+objRs.item_recid+helpers.getPODateItem() //temrecid + MMDDYYYY
        +'&supplier_recid='+sID
        +'&item_qty='+this.state.orderVals[id]
        +'&item_recid='+objRs.item_recid
        +'&createdby='+parseInt(this.idEmployee)
        +'&approver='+parseInt(this.idEmployee);
      // console.log(url); return;
      fetch(url, { method:'post', headers:rest.HEADERSP 
      }).then((response) => response.json()).then((rxJson) => {
        // console.log(rxJson); return;
        if (rxJson[0].error.toString() == "false") {
          if (data.length == 0) {
            db.addPoRefs([{supplierID:''+sID,porecid:poID}]).then((data) => { 
              this.refs.sToast.show('PO SUCCESS!\nItem successfully processed.');
            }).catch((err) => { console.error(err); });
          } else {
            this.refs.sToast.show('PO SUCCESS!\nItem successfully processed.');
          }
        } else {
          this.refs.eToast.show('PO ERROR!\nPlease contact IT support.');
        }
        this.setState({ ldrLft:width });
      });
    });
  }

  getDbSuppliers = (strSearch) => { 
    if (getSuppLoadingFlag == 1) return; getSuppLoadingFlag = 1; // blocking double call
    db.getRset(cons.TBL.SUPP, "WHERE suppliername LIKE ? ", "", ['%'+strSearch+'%'])
    .then((jsn) => {
      this.setState({sData:[]});
      this.setState({sData:jsn});
    }).catch((err) => { 
      console.error(err); 
      alert("Error on Request! Please inform DEVELOPER....");
    });
  }

}

export default Home;

class RowSupplierR extends PureComponent {
  constructor(props){ super(props); }
  render = () => {
    const { item, index, getDbSuppliers } = this.props;
    return (
      <View style={{height:100,alignItems:'center',justifyContent:'center',flex:1,backgroundColor:'rgba(64,83,35,0.4)',
        borderRadius:7,paddingHorizontal:15,borderColor:'#fad2de',borderWidth:1}} 
        key={index}>
        <Text style={{color:'#fad2de',fontSize:20,fontFamily:'Lato-Regular',textAlign:'center'}}>{item.suppliername}</Text>
      </View>
    )
  } 
}

class RowSupplier extends PureComponent {
  render = () => {
    const { item } = this.props;
    return (
      <View style={{height:40,alignItems:'center',justifyContent:'center',flex:1,backgroundColor:'#405323',marginStart:5,marginEnd:4,marginTop:7,
        borderTopEndRadius:5,borderTopLeftRadius:5}} >
        <Text style={{color:'#fad2de',fontSize:17,fontFamily:'Lato-Bold'}}>{item.suppliername}</Text>
      </View>
    )
  } 
}

class RowHeader extends PureComponent {
  render = () => {
    return (
      <View style={{flexDirection:'row',marginStart:5,marginEnd:4}}>
        <View style={styles.tblHPre}><Text style={styles.txtCl}>ITEM NAME</Text></View>
        <View style={styles.tblH}><Text style={styles.txtCl}>UNIT</Text></View>
        <View style={styles.tblH}><Text style={styles.txtCl}>B1</Text></View>
        <View style={styles.tblH}><Text style={styles.txtCl}>B2</Text></View>
        <View style={styles.tblH}><Text style={styles.txtCl}>B3</Text></View>
        <View style={styles.tblH}><Text style={styles.txtCl}>COLD</Text></View>
        <View style={styles.tblH}><Text style={styles.txtCl}>TOTAL</Text></View>
        <View style={styles.tblH}><Text style={styles.txtCl}>USED</Text></View>
        <View style={styles.tblH}><Text style={styles.txtCl}>PENDING</Text></View>
        <View style={styles.tblH}><Text style={styles.txtCl}>ORDER</Text></View>
      </View>
    )
  }
}

class RowItem extends PureComponent {
  render = () => {
    const { item, orderVals, trigger } = this.props;
    return (
      <View style={{flexDirection:'row'}} trigger={trigger}>
        <View style={styles.tblCPre}>
          <Text style={styles.txtC}>{item.itemname}</Text>
        </View>
        <View style={styles.tblCc}>
          <Text style={styles.txtC}>{item.unit}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.B1}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.B2}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.B3}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.cold_two}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{helpers.nWithCommas(
            helpers.formatStrToInt(item.B1) + helpers.formatStrToInt(item.B2) + helpers.formatStrToInt(item.B3) + helpers.formatStrToInt(item.cold_two)
          )}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.used}</Text>
        </View>
        <View style={styles.tblC}>
          <Text style={styles.txtC}>{item.pending}</Text>
        </View>
        <View style={styles.tblCc}>
          <View style={{flex:1,margin:2,width:'100%',justifyContent:'center',alignItems:'center'}}>
            <TextInput style={{color:"gold",fontSize:16,fontFamily:'Lato-Regular',paddingTop:0,paddingBottom:0,paddingStart:0,paddingEnd:0,marginStart:0,marginEnd:0,width:'100%',textAlign:'center'}} placeholder={item.ordero}  value={orderVals[item.rid]} 
              editable={false} placeholderTextColor='gray' />
          </View>
        </View>
      </View>
    )
  }
}


  var xample = [{"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 0, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "1017 Merchandise Store / VIOLETA ALFONSO", "type": "1017 Merchandise Store / VIOLETA ALFONSO", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 1, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2009254074, "itemname": "Net 5.5m X 6m (Pc)", "order": "", "ordero": "20", "pending": "", "rid": 2, "saved": 0, "selected": 0, "supplierID": 1811230141, "suppliername": "1017 Merchandise Store / VIOLETA ALFONSO", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "915", "B3": "", "Total": "", "cold_two": "", "item_recid": 1810454300, "itemname": "Bilao Native (Large) (Pc)", "order": "", "ordero": "8", "pending": "", "rid": 3, "saved": 0, "selected": 0, "supplierID": 1811230141, "suppliername": "1017 Merchandise Store / VIOLETA ALFONSO", "type": "item", "unit": "pc", "used": "205.5"}, {"B1": "", "B2": "895", "B3": "", "Total": "", "cold_two": "", "item_recid": 1810460291, "itemname": "Bilao Native (Medium) (Pc)", "order": "", "ordero": "89", "pending": "", "rid": 4, "saved": 0, "selected": 0, "supplierID": 1811230141, "suppliername": "1017 Merchandise Store / VIOLETA ALFONSO", "type": "item", "unit": "pc", "used": "113.75"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 5, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "3M INDUSTRIAL SUPPLY", "type": "3M INDUSTRIAL SUPPLY", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 6, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1911553776, "itemname": "Pig Tale (1/4) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 7, "saved": 0, "selected": 0, "supplierID": 1911543087, "suppliername": "3M INDUSTRIAL SUPPLY", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 8, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "A Vic International Trading Corp.", "type": "A Vic International Trading Corp.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 9, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "11", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161970, "itemname": "Food Color (Masterchef) (Golden Yellow) (12 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 10, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "9", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161974, "itemname": "Food Color (Masterchef) (Leaf Green) (12 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 11, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "12", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161978, "itemname": "Food Color (Masterchef) (Sunset Orange) (12 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 12, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "4", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912380934, "itemname": "Food Color (Masterchef) (Lemon Yellow) (12 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 13, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "Case", "used": ".31"}, {"B1": "", "B2": "5", "B3": "", "Total": "", "cold_two": "", "item_recid": 1913093366, "itemname": "Food Color (Masterchef) (Violet) (12 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 14, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 20092810482361, "itemname": "Master Gel (Masterchef) (Rose Pink) (20g) (Jar)", "order": "", "ordero": "", "pending": "", "rid": 15, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "jar", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161968, "itemname": "Food Color (Masterchef) (Golden Yellow) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 16, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "4", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161972, "itemname": "Food Color (Masterchef) (Leaf Green) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 17, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "pc", "used": ".5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161976, "itemname": "Food Color (Masterchef) (Sunset Orange) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 18, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912374679, "itemname": "Food Color (Masterchef) (Lemon Yellow) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 19, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1913092280, "itemname": "Food Color (Masterchef) (Violet) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 20, "saved": 0, "selected": 0, "supplierID": 1913060531, "suppliername": "A Vic International Trading Corp.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 21, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "A2A1 BREADS O' CAKES BAKESHOP", "type": "A2A1 BREADS O' CAKES BAKESHOP", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 22, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912070517, "itemname": "Cake (Strawberry) (Cheesecake) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 23, "saved": 0, "selected": 0, "supplierID": 1912133227, "suppliername": "A2A1 BREADS O' CAKES BAKESHOP", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912074096, "itemname": "Cake (Blueberry) (Cheesecake) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 24, "saved": 0, "selected": 0, "supplierID": 1912133227, "suppliername": "A2A1 BREADS O' CAKES BAKESHOP", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912082512, "itemname": "Cake (Oreo) (Cheesecake) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 25, "saved": 0, "selected": 0, "supplierID": 1912133227, "suppliername": "A2A1 BREADS O' CAKES BAKESHOP", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912085936, "itemname": "Cake (Sinful) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 26, "saved": 0, "selected": 0, "supplierID": 1912133227, "suppliername": "A2A1 BREADS O' CAKES BAKESHOP", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912095743, "itemname": "Cake (Redvelvet) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 27, "saved": 0, "selected": 0, "supplierID": 1912133227, "suppliername": "A2A1 BREADS O' CAKES BAKESHOP", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912113914, "itemname": "Cake (Doublefudge) (Cheesecake) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 28, "saved": 0, "selected": 0, "supplierID": 1912133227, "suppliername": "A2A1 BREADS O' CAKES BAKESHOP", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912121027, "itemname": "Cake (Decadent) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 29, "saved": 0, "selected": 0, "supplierID": 1912133227, "suppliername": "A2A1 BREADS O' CAKES BAKESHOP", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 30, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "AAB Baking Goods and Supplies Inc.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 31, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "384", "B2": "10", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814230324, "itemname": "Muffin Cup (Green) (3oz) (3,600 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 32, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "118", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814312814, "itemname": "Muffin Cup (Yellow) (3oz) (3,600 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 33, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814343077, "itemname": "Muffin Cup (Brownies) (Sq-45) (10 Pack /1)", "order": "", "ordero": "", "pending": "", "rid": 34, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "4", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912261521, "itemname": "Microwavable Container (Aab Baking Goods) (750tw 31oz) (Rect Twin) (300 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 35, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "24", "B2": "3", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814341108, "itemname": "Foil Cup (165c) (5,000 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 36, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "Case", "used": "3"}, {"B1": "20", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2009134194, "itemname": "Cellophane (7x7) (Violet) (Pack)", "order": "", "ordero": "10", "pending": "", "rid": 37, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912263542, "itemname": "Microwavable Container (Aab Baking Goods) (750tw 31oz) (Rect Twin) (60 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 38, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814342147, "itemname": "Muffin Cup (Brownies) (Sq-45) (5,000 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 39, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "20", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815092202, "itemname": "Cellophane (7x7) (Blue) (Pack)", "order": "", "ordero": "78", "pending": "", "rid": 40, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "20", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815095528, "itemname": "Cellophane (7x7) (Green) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 41, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815103048, "itemname": "Cellophane (7x7) (Yellow) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 42, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915185837, "itemname": "Sprinkles (Aab Baking Goods) (Ube) (Kg) (Pack)", "order": "", "ordero": "", "pending": "5", "rid": 43, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915192953, "itemname": "Sprinkles (Aab Baking Goods) (Yellow) (Kg) (Pack)", "order": "", "ordero": "", "pending": "5", "rid": 44, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915244940, "itemname": "Assorted Sprinkles (Aab Baking Goods) (Kg) (Pack)", "order": "", "ordero": "", "pending": "10", "rid": 45, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "48", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2015384215, "itemname": "Oreo (Regular) (266g) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 46, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "50", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1908300532, "itemname": "Candle (Happy Birthday) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 47, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915374277, "itemname": "Kitchen Scale (14192-466f) (60kg) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 48, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "8", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2015534393, "itemname": "Coupler (Standard) (4 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 49, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814225545, "itemname": "Muffin Cup (Green) (3oz) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 50, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "802,800", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814311285, "itemname": "Muffin Cup (Yellow) (3oz) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 51, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "140,000", "B2": "65,000", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814333265, "itemname": "Muffin Cup (Brownies) (Sq-45) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 52, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": "7,500"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1910502596, "itemname": "Baking Pan (Aab Baking Goods) (Square) (8 X 8) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 53, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "500,000", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814334563, "itemname": "Foil Cup (165c) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 54, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815272352, "itemname": "Paper (Aab Baking Goods) (Saga Parchment) (60 X 40 Cm) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 55, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915254331, "itemname": "Candle (Aab Baking Goods) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 56, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915270851, "itemname": "Pet Cup (Aab Baking Goods) (With Lid) (12oz) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 57, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2010403147, "itemname": "Shortening (Snow White) (40kg) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 58, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "kg", "used": "1"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1807255710, "itemname": "Baking Pan (Aab Baking Goods) (Rectangle) (8 X 12) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 59, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "18,900", "B3": "", "Total": "", "cold_two": "", "item_recid": 1914325415, "itemname": "Paper Straw (Green) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 60, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": "250"}, {"B1": "", "B2": "900", "B3": "", "Total": "", "cold_two": "", "item_recid": 1916590744, "itemname": "Rob12 Sinantan (Aab Baking Goods) (Container) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 61, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "29,750", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815564681, "itemname": "Box (Ensaymada) (9 X 12 X 3) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 62, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": "562.5"}, {"B1": "", "B2": "1,600", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816040937, "itemname": "Halo - Halo (Container) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 63, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": "62.5"}, {"B1": "", "B2": "1,600", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816060075, "itemname": "Halo - Halo (Dome Lid) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 64, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": "62.5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912170954, "itemname": "Microwavable Container (Aab Baking Goods) (750tw 31oz) (Rect Twin) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 65, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "70", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815274039, "itemname": "Paper (Aab Baking Goods) (Saga Parchment) (60 X 40 Cm) (500 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 66, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "Ream", "used": "1.5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1914033789, "itemname": "Rice Crispies (15kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 67, "saved": 0, "selected": 0, "supplierID": 1817074859, "suppliername": "AAB Baking Goods and Supplies Inc.", "type": "item", "unit": "Sack", "used": ".25"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 68, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "ABLAZE Marketing", "type": "ABLAZE Marketing", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 69, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1916494404, "itemname": "Epson Sjic23p (Black) (For Tm C3510) (Pc)", "order": "", "ordero": "", "pending": "10", "rid": 70, "saved": 0, "selected": 0, "supplierID": 1916470191, "suppliername": "ABLAZE Marketing", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1916502209, "itemname": "Epson Sjic23p (Cyan) (For Tm C3510) (Pc)", "order": "", "ordero": "", "pending": "10", "rid": 71, "saved": 0, "selected": 0, "supplierID": 1916470191, "suppliername": "ABLAZE Marketing", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1916505181, "itemname": "Epson Sjic23p (Magenta) (For Tm C3510) (Pc)", "order": "", "ordero": "", "pending": "10", "rid": 72, "saved": 0, "selected": 0, "supplierID": 1916470191, "suppliername": "ABLAZE Marketing", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1916512025, "itemname": "Epson Sjic23p (Yellow) (For Tm C3510) (Pc)", "order": "", "ordero": "", "pending": "10", "rid": 73, "saved": 0, "selected": 0, "supplierID": 1916470191, "suppliername": "ABLAZE Marketing", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1917012139, "itemname": "Epson Sjic23p (Maintennance Box) (For Tm C3510) (Pc)", "order": "", "ordero": "", "pending": "2", "rid": 74, "saved": 0, "selected": 0, "supplierID": 1916470191, "suppliername": "ABLAZE Marketing", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 75, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Absolute Sales Corporation", "type": "Absolute Sales Corporation", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 76, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1808274637, "itemname": "Mineral Water (Summit) (500ml) (Bottle)", "order": "", "ordero": "", "pending": "", "rid": 77, "saved": 0, "selected": 0, "supplierID": 1816293128, "suppliername": "Absolute Sales Corporation", "type": "item", "unit": "Bottle", "used": ""}, {"B1": "", "B2": "34", "B3": "", "Total": "", "cold_two": "", "item_recid": 1808275922, "itemname": "Mineral Water (Summit) (500ml) (24 Bottle /1)", "order": "", "ordero": "", "pending": "50", "rid": 78, "saved": 0, "selected": 0, "supplierID": 1816293128, "suppliername": "Absolute Sales Corporation", "type": "item", "unit": "Case", "used": "3.25"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 79, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "ACCESS LIFT AND ELECTRONICS CORP.", "type": "ACCESS LIFT AND ELECTRONICS CORP.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 80, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1911304364, "itemname": "Digital Display & Decoder Four Sets (Pc)", "order": "", "ordero": "", "pending": "", "rid": 81, "saved": 0, "selected": 0, "supplierID": 1911292716, "suppliername": "ACCESS LIFT AND ELECTRONICS CORP.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 82, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Ace Hardware", "type": "Ace Hardware", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 83, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2017540743, "itemname": "All Purpose Cleaner (Spotless) (Cont)", "order": "", "ordero": "", "pending": "", "rid": 84, "saved": 0, "selected": 0, "supplierID": 2011265746, "suppliername": "Ace Hardware", "type": "item", "unit": "Cont", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2008315932, "itemname": "Battery Aa ( Ace ) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 85, "saved": 0, "selected": 0, "supplierID": 2011265746, "suppliername": "Ace Hardware", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2008332195, "itemname": "Battery Aaa ( Ace ) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 86, "saved": 0, "selected": 0, "supplierID": 2011265746, "suppliername": "Ace Hardware", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 87, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Ads Empire Corporation", "type": "Ads Empire Corporation", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 88, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2014005646, "itemname": "Sticker (Window) (16.5 X 13.5) (Pc)", "order": "", "ordero": "", "pending": "1", "rid": 89, "saved": 0, "selected": 0, "supplierID": 2014000776, "suppliername": "Ads Empire Corporation", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 90, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Advance Paper Corporation ", "type": "Advance Paper Corporation ", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 91, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814173207, "itemname": "Paper Bag (Advance) (#12 With Print) (10 Pack /1)", "order": "", "ordero": "", "pending": "", "rid": 92, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "Bundle", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814181997, "itemname": "Paper Bag (Advance) (#5 With Print) (10 Pack /1)", "order": "", "ordero": "", "pending": "", "rid": 93, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "Bundle", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814525636, "itemname": "Paper Bag (Advance) (#12 With Print) (100 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 94, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814554806, "itemname": "Paper Bag (Advance) (#5 With Print) (100 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 95, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815251384, "itemname": "Record Book (300 Pages) (Pc)", "order": "", "ordero": "", "pending": "100", "rid": 96, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "pc", "used": ""}, {"B1": "40,000", "B2": "4,700", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814524193, "itemname": "Paper Bag (Advance) (#12 With Print) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 97, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "pc", "used": "2,575"}, {"B1": "69,000", "B2": "6,600", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814550493, "itemname": "Paper Bag (Advance) (#5 With Print) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 98, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "pc", "used": "1,525"}, {"B1": "40", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815380766, "itemname": "Bond Paper ( Long ) (Ream)", "order": "", "ordero": "", "pending": "", "rid": 99, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "Ream", "used": ""}, {"B1": "100", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815385197, "itemname": "Bond Paper ( Short ) (Ream)", "order": "", "ordero": "", "pending": "", "rid": 100, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "Ream", "used": ".25"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2014240177, "itemname": "Bond Paper (Hard Copy) (Long) (Ream)", "order": "", "ordero": "", "pending": "", "rid": 101, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "Ream", "used": "17.5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2014245583, "itemname": "Bond Paper (Hard Copy) (Short) (Ream)", "order": "", "ordero": "", "pending": "", "rid": 102, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "Ream", "used": "50"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2017151570, "itemname": "Bond Paper (Hard Copy) (A4) (Ream)", "order": "", "ordero": "", "pending": "", "rid": 103, "saved": 0, "selected": 0, "supplierID": 1815271726, "suppliername": "Advance Paper Corporation ", "type": "item", "unit": "Ream", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 104, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "Advance Printing and Packaging Solutions Inc.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 105, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "35,258", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814213103, "itemname": "Box (Buko Pandan Salad) (Small) (7 1/4 X 4 3/4 X 3 1/4) (50 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 106, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "400", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2015571817, "itemname": "Apron (Washable) (Clothes Shield) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 107, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "29,572", "B2": "300", "B3": "", "Total": "", "cold_two": "", "item_recid": 1810431958, "itemname": "Box (Brownies 20) (10.5x8x1.25) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 108, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": "200"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1810464072, "itemname": "Box (Buko Pandan Salad) (8x8x3.5) (Big) (Pc)", "order": "", "ordero": "", "pending": "20,000", "rid": 109, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": "150"}, {"B1": "46,200", "B2": "700", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814211901, "itemname": "Box (Buko Pandan Salad) (Small) (7 1/4 X 4 3/4 X 3 1/4) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 110, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": "250"}, {"B1": "17,000", "B2": "800", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814271296, "itemname": "Box (Cassava) (Small) (8x4x2) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 111, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": "675"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814372192, "itemname": "Box (Polvoron) (6 X 4 1/2 X 3) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 112, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2016130652, "itemname": "Box (Advance Printing) (10x8x3) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 113, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "-50", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814444424, "itemname": "Box (Pie) (9x6x1.25) (Pc)", "order": "", "ordero": "", "pending": "10,000", "rid": 114, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": "1,187.5"}, {"B1": "76,625", "B2": "1,650", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814513640, "itemname": "Box (Siomai 50/cassava Big) (8x8x2) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 115, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": "212.5"}, {"B1": "24,300", "B2": "5,700", "B3": "", "Total": "", "cold_two": "", "item_recid": 1911331936, "itemname": "Paper Bag (Advance Printing) (With Handle) (With Board) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 116, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": "2,450"}, {"B1": "4,125", "B2": "2,500", "B3": "", "Total": "", "cold_two": "", "item_recid": 1911560505, "itemname": "Meal Box (Spaghetti) (Advance Printing) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 117, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": "575"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912105461, "itemname": "Paper Bag (With Handle) (W/out Board) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 118, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "20,900", "B3": "", "Total": "", "cold_two": "", "item_recid": 1914533620, "itemname": "Paper Wrap (Rice) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 119, "saved": 0, "selected": 0, "supplierID": 1912074878, "suppliername": "Advance Printing and Packaging Solutions Inc.", "type": "item", "unit": "pc", "used": "37.5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 120, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Aiza's Sweets", "type": "Aiza's Sweets", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 121, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161696, "itemname": "Marzipan (Aizas) (50 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 122, "saved": 0, "selected": 0, "supplierID": 1814481302, "suppliername": "Aiza's Sweets", "type": "item", "unit": "Box", "used": "25"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161692, "itemname": "Marzipan (Aizas) (6 Pc /1)", "order": "", "ordero": "", "pending": "300", "rid": 123, "saved": 0, "selected": 0, "supplierID": 1814481302, "suppliername": "Aiza's Sweets", "type": "item", "unit": "pack", "used": "100"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161694, "itemname": "Marzipan (Aizas) (16 Pc /1)", "order": "", "ordero": "", "pending": "100", "rid": 124, "saved": 0, "selected": 0, "supplierID": 1814481302, "suppliername": "Aiza's Sweets", "type": "item", "unit": "pack", "used": "87.5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 125, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Ajinomoto Philippines Corporation", "type": "Ajinomoto Philippines Corporation", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 126, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1808523354, "itemname": "Ginisa Mix (Ajinomoto) (250g) (36 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 127, "saved": 0, "selected": 0, "supplierID": 1815454615, "suppliername": "Ajinomoto Philippines Corporation", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "14", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814472217, "itemname": "Msg (Ajinomoto) (Lc) (Kg) (12 Pack /1)", "order": "", "ordero": "", "pending": "10", "rid": 128, "saved": 0, "selected": 0, "supplierID": 1815454615, "suppliername": "Ajinomoto Philippines Corporation", "type": "item", "unit": "Case", "used": "2.25"}, {"B1": "", "B2": "21", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814481565, "itemname": "Msg (Ajinomoto) (Rc) (Kg) (12 Pack /1)", "order": "", "ordero": "", "pending": "5", "rid": 129, "saved": 0, "selected": 0, "supplierID": 1815454615, "suppliername": "Ajinomoto Philippines Corporation", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814471093, "itemname": "Msg (Ajinomoto) (Lc) (Kg) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 130, "saved": 0, "selected": 0, "supplierID": 1815454615, "suppliername": "Ajinomoto Philippines Corporation", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814480578, "itemname": "Msg (Ajinomoto) (Rc) (Kg) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 131, "saved": 0, "selected": 0, "supplierID": 1815454615, "suppliername": "Ajinomoto Philippines Corporation", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 132, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Albert Ariola", "type": "Albert Ariola", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 133, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1812440260, "itemname": "Calamansi (Kg) (Kilo)", "order": "", "ordero": "", "pending": "30", "rid": 134, "saved": 0, "selected": 0, "supplierID": 1912260002, "suppliername": "Albert Ariola", "type": "item", "unit": "Kilo", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1910270297, "itemname": "Calamansi (Kg) (30 Kilo /1)", "order": "", "ordero": "", "pending": "32", "rid": 135, "saved": 0, "selected": 0, "supplierID": 1912260002, "suppliername": "Albert Ariola", "type": "item", "unit": "Sack", "used": "30"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 136, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Alejandro Co", "type": "Alejandro Co", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 137, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1815002155, "itemname": "Ensalada (Nathaniels) (Ampalaya) (Cont)", "order": "", "ordero": "", "pending": "60", "rid": 138, "saved": 0, "selected": 0, "supplierID": 1808322862, "suppliername": "Alejandro Co", "type": "item", "unit": "Cont", "used": "16.5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 139, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Alexander Sauce Supply", "type": "Alexander Sauce Supply", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 140, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814393295, "itemname": "Worcestershire (Alexander) (Gal) (4 Pc /1)", "order": "", "ordero": "", "pending": "5", "rid": 141, "saved": 0, "selected": 0, "supplierID": 1815275575, "suppliername": "Alexander Sauce Supply", "type": "item", "unit": "Case", "used": "1.75"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814392199, "itemname": "Worcestershire (Alexander) (Gal) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 142, "saved": 0, "selected": 0, "supplierID": 1815275575, "suppliername": "Alexander Sauce Supply", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 143, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "All Sufficient Medical Supply & Pharmacy", "type": "All Sufficient Medical Supply & Pharmacy", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 144, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2010003626, "itemname": "Graduated Cylinder (100ml) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 145, "saved": 0, "selected": 0, "supplierID": 2009591254, "suppliername": "All Sufficient Medical Supply & Pharmacy", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 146, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Allies Wonder Peanuts", "type": "Allies Wonder Peanuts", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 147, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1809192242, "itemname": "Peanut (Allies) (450g) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 148, "saved": 0, "selected": 0, "supplierID": 1814473909, "suppliername": "Allies Wonder Peanuts", "type": "item", "unit": "pc", "used": "12.5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161780, "itemname": "Cashew (Allies) (100g) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 149, "saved": 0, "selected": 0, "supplierID": 1814473909, "suppliername": "Allies Wonder Peanuts", "type": "item", "unit": "pc", "used": "25"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161782, "itemname": "Cashew (Allies) (375g) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 150, "saved": 0, "selected": 0, "supplierID": 1814473909, "suppliername": "Allies Wonder Peanuts", "type": "item", "unit": "pc", "used": "25"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 151, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "All-Technik and Components, Inc.", "type": "All-Technik and Components, Inc.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 152, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915160171, "itemname": "Polyurethane Hose (10mm) (Pu10065-B4) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 153, "saved": 0, "selected": 0, "supplierID": 1911211362, "suppliername": "All-Technik and Components, Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 154, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Aloha's Chicharon / ALOHA DAYRIT", "type": "Aloha's Chicharon / ALOHA DAYRIT", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 155, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161942, "itemname": "Chicharon (Nathaniels) (Plain) (Pack)", "order": "", "ordero": "", "pending": "100", "rid": 156, "saved": 0, "selected": 0, "supplierID": 1816384849, "suppliername": "Aloha's Chicharon / ALOHA DAYRIT", "type": "item", "unit": "pack", "used": "550"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161948, "itemname": "Chicharon (Nathaniels) (Spicy) (Pack)", "order": "", "ordero": "", "pending": "100", "rid": 157, "saved": 0, "selected": 0, "supplierID": 1816384849, "suppliername": "Aloha's Chicharon / ALOHA DAYRIT", "type": "item", "unit": "pack", "used": "475"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 158, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Amazon Manufacturing Corporation ", "type": "Amazon Manufacturing Corporation ", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 159, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915424709, "itemname": "Bread Tray (Navy Blue) (Pc)", "order": "", "ordero": "", "pending": "150", "rid": 160, "saved": 0, "selected": 0, "supplierID": 1809361534, "suppliername": "Amazon Manufacturing Corporation ", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915435294, "itemname": "Bread Tray (Moss Green) (Pc)", "order": "", "ordero": "", "pending": "300", "rid": 161, "saved": 0, "selected": 0, "supplierID": 1809361534, "suppliername": "Amazon Manufacturing Corporation ", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1917212575, "itemname": "Bread Tray (Grey) (Pc)", "order": "", "ordero": "", "pending": "50", "rid": 162, "saved": 0, "selected": 0, "supplierID": 1809361534, "suppliername": "Amazon Manufacturing Corporation ", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1917221763, "itemname": "Bread Tray (Orange) (Pc)", "order": "", "ordero": "", "pending": "50", "rid": 163, "saved": 0, "selected": 0, "supplierID": 1809361534, "suppliername": "Amazon Manufacturing Corporation ", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 164, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Angeles Pima Press Corporation", "type": "Angeles Pima Press Corporation", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 165, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1913390520, "itemname": "Sales Invoice (Og) (Booklet)", "order": "", "ordero": "", "pending": "400", "rid": 166, "saved": 0, "selected": 0, "supplierID": 1911401742, "suppliername": "Angeles Pima Press Corporation", "type": "item", "unit": "Booklet", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 167, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Arnold Manaloto", "type": "Arnold Manaloto", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 168, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1810025351, "itemname": "Egg (Salted) (Pc)", "order": "", "ordero": "", "pending": "3,000", "rid": 169, "saved": 0, "selected": 0, "supplierID": 1816390727, "suppliername": "Arnold Manaloto", "type": "item", "unit": "pc", "used": "4,875"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 170, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Arrow Manufacturing Corp", "type": "Arrow Manufacturing Corp", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 171, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1808404103, "itemname": "Cake Pan (Arrow) (8 Inches) (Round) (Pc)", "order": "", "ordero": "", "pending": "100", "rid": 172, "saved": 0, "selected": 0, "supplierID": 1813555380, "suppliername": "Arrow Manufacturing Corp", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814113748, "itemname": "Muffin Cup (Arrow) (6 Holes) (Pc)", "order": "", "ordero": "", "pending": "300", "rid": 173, "saved": 0, "selected": 0, "supplierID": 1813555380, "suppliername": "Arrow Manufacturing Corp", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1807244001, "itemname": "Baking Pan (Arrow) (Round) (8 X 1.5) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 174, "saved": 0, "selected": 0, "supplierID": 1813555380, "suppliername": "Arrow Manufacturing Corp", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1811193061, "itemname": "Pan (Arrow) (Muffin) (Large) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 175, "saved": 0, "selected": 0, "supplierID": 1813555380, "suppliername": "Arrow Manufacturing Corp", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1910472380, "itemname": "Baking Pan (Arrow) (Round) (8 X 2.5) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 176, "saved": 0, "selected": 0, "supplierID": 1813555380, "suppliername": "Arrow Manufacturing Corp", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1910481593, "itemname": "Cooking Sheet (Arrow) (13 X 18) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 177, "saved": 0, "selected": 0, "supplierID": 1813555380, "suppliername": "Arrow Manufacturing Corp", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 178, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Ascorp Inc.", "type": "Ascorp Inc.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 179, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161688, "itemname": "Egg (Raw) (Large) (12 Tray /1)", "order": "", "ordero": "", "pending": "", "rid": 180, "saved": 0, "selected": 0, "supplierID": 1816353584, "suppliername": "Ascorp Inc.", "type": "item", "unit": "Case", "used": "45.5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161705, "itemname": "Egg (Raw) (Medium) (12 Tray /1)", "order": "", "ordero": "", "pending": "", "rid": 181, "saved": 0, "selected": 0, "supplierID": 1816353584, "suppliername": "Ascorp Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161686, "itemname": "Egg (Raw) (Large) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 182, "saved": 0, "selected": 0, "supplierID": 1816353584, "suppliername": "Ascorp Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161703, "itemname": "Egg (Raw) (Medium) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 183, "saved": 0, "selected": 0, "supplierID": 1816353584, "suppliername": "Ascorp Inc.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 184, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Asia Global Plast Mfg. Corp.", "type": "Asia Global Plast Mfg. Corp.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 185, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1910463932, "itemname": "Container (Asia Global Plastic) (Kakanin) (47 X 35 X 44 Cm) (Oval 400) (300 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 186, "saved": 0, "selected": 0, "supplierID": 1816455182, "suppliername": "Asia Global Plast Mfg. Corp.", "type": "item", "unit": "Box", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816394632, "itemname": "S10 (Asia Global Plastic) (Ro/250) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 187, "saved": 0, "selected": 0, "supplierID": 1816455182, "suppliername": "Asia Global Plast Mfg. Corp.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816401475, "itemname": "S16 (Asia Global Plastic) (Ro/450) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 188, "saved": 0, "selected": 0, "supplierID": 1816455182, "suppliername": "Asia Global Plast Mfg. Corp.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816403985, "itemname": "S30 (Asia Global Plastic) (Ro/750) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 189, "saved": 0, "selected": 0, "supplierID": 1816455182, "suppliername": "Asia Global Plast Mfg. Corp.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816441227, "itemname": "Microwavable Container (Re/750) (With Lid) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 190, "saved": 0, "selected": 0, "supplierID": 1816455182, "suppliername": "Asia Global Plast Mfg. Corp.", "type": "item", "unit": "pc", "used": ""}, {"B1": "7,500", "B2": "1,850", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816444088, "itemname": "Microwavable Container (Re/500) (With Lid) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 191, "saved": 0, "selected": 0, "supplierID": 1816455182, "suppliername": "Asia Global Plast Mfg. Corp.", "type": "item", "unit": "pc", "used": "12.5"}, {"B1": "52,200", "B2": "3,000", "B3": "", "Total": "", "cold_two": "", "item_recid": 1910461930, "itemname": "Container (Asia Global Plastic) (Kakanin) (47 X 35 X 44 Cm) (Oval 400) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 192, "saved": 0, "selected": 0, "supplierID": 1816455182, "suppliername": "Asia Global Plast Mfg. Corp.", "type": "item", "unit": "pc", "used": "1,125"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 193, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Asiacold Storage Corporation ", "type": "Asiacold Storage Corporation ", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 194, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912542295, "itemname": "Walk-In Cold Storage Freezer (Pc)", "order": "", "ordero": "", "pending": "", "rid": 195, "saved": 0, "selected": 0, "supplierID": 1812483339, "suppliername": "Asiacold Storage Corporation ", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1911530720, "itemname": "Oil Separator (Pc)", "order": "", "ordero": "", "pending": "2", "rid": 196, "saved": 0, "selected": 0, "supplierID": 1812483339, "suppliername": "Asiacold Storage Corporation ", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 197, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Attivo Realty Development Corp.", "type": "Attivo Realty Development Corp.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 198, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1910042676, "itemname": "Microwave Oven (Whirlpool) (Mwx203bl) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 199, "saved": 0, "selected": 0, "supplierID": 1811285564, "suppliername": "Attivo Realty Development Corp.", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 200, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Axelum Resources Corporation", "type": "Axelum Resources Corporation", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 201, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "2", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161679, "itemname": "Dessicated Coconut (Red V) (50lb) (Sack)", "order": "", "ordero": "", "pending": "3", "rid": 202, "saved": 0, "selected": 0, "supplierID": 1815380503, "suppliername": "Axelum Resources Corporation", "type": "item", "unit": "Sack", "used": ".75"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 203, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Bahama Bean Curd Products", "type": "Bahama Bean Curd Products", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 204, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1809305683, "itemname": "Tokwa (Pc)", "order": "", "ordero": "", "pending": "", "rid": 205, "saved": 0, "selected": 0, "supplierID": 1914513824, "suppliername": "Bahama Bean Curd Products", "type": "item", "unit": "pc", "used": "37.5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 206, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Bakels Philippines, Inc.", "type": "Bakels Philippines, Inc.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 207, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814425477, "itemname": "Frying Fat (Panville) (20kg) (20 Kilo /1)", "order": "", "ordero": "", "pending": "", "rid": 208, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "1", "B3": "", "Total": "", "cold_two": "", "item_recid": 1809420865, "itemname": "Monofresh (Bakels) (Kg) (12 Pack /1)", "order": "", "ordero": "", "pending": "", "rid": 209, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "39", "B3": "", "Total": "", "cold_two": "", "item_recid": 1810292946, "itemname": "Shortening (Rotitex) (20kg) (Case)", "order": "", "ordero": "", "pending": "", "rid": 210, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "Case", "used": "7"}, {"B1": "", "B2": "4", "B3": "", "Total": "", "cold_two": "", "item_recid": 1811261951, "itemname": "Paste (Apito) (Ube) (2kg) (8 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 211, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "3", "B3": "", "Total": "", "cold_two": "", "item_recid": 1817510920, "itemname": "Csp 60 (Bakels) (Anti-Amag) (Kg) (12 Pack /1)", "order": "", "ordero": "", "pending": "", "rid": 212, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "2", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161683, "itemname": "Dobrim (Bakels) (Kg) (12 Pack /1)", "order": "", "ordero": "", "pending": "", "rid": 213, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814193321, "itemname": "Lecinta Plus (Bakels) (Kg) (12 Pack /1)", "order": "", "ordero": "", "pending": "", "rid": 214, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162473, "itemname": "Paste (Apito) (Chocolate) (1kg) (15 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 215, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816520994, "itemname": "Frying Fat (Panville) (20kg) (Kilo)", "order": "", "ordero": "", "pending": "", "rid": 216, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "Kilo", "used": ""}, {"B1": "", "B2": "2", "B3": "", "Total": "", "cold_two": "", "item_recid": 1809415587, "itemname": "Monofresh (Bakels) (Kg) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 217, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "pack", "used": "1.75"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161681, "itemname": "Dobrim (Bakels) (Kg) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 218, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "pack", "used": "2"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814192288, "itemname": "Lecinta Plus (Bakels) (Kg) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 219, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "7", "B3": "", "Total": "", "cold_two": "", "item_recid": 1817505696, "itemname": "Csp 60 (Bakels) (Anti-Amag) (Kg) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 220, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "pack", "used": "1.75"}, {"B1": "", "B2": "5", "B3": "", "Total": "", "cold_two": "", "item_recid": 1811255943, "itemname": "Paste (Apito) (Ube) (2kg) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 221, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "pc", "used": ".25"}, {"B1": "", "B2": "-2", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162471, "itemname": "Paste (Apito) (Chocolate) (1kg) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 222, "saved": 0, "selected": 0, "supplierID": 1816121138, "suppliername": "Bakels Philippines, Inc.", "type": "item", "unit": "pc", "used": ".5"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 223, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Bakerspride  Co., Inc", "type": "Bakerspride  Co., Inc", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 224, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "22", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162477, "itemname": "Chocolate (Vivo) (Fancy Dark) (3kg) (12 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 225, "saved": 0, "selected": 0, "supplierID": 1815225270, "suppliername": "Bakerspride  Co., Inc", "type": "item", "unit": "Case", "used": ""}, {"B1": "7", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162482, "itemname": "Chocolate (Vivo) (Fancy White) (3kg) (12 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 226, "saved": 0, "selected": 0, "supplierID": 1815225270, "suppliername": "Bakerspride  Co., Inc", "type": "item", "unit": "Case", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1912361267, "itemname": "Chocolate (Bakerspride) (White Choco) (Kilo)", "order": "", "ordero": "", "pending": "5", "rid": 227, "saved": 0, "selected": 0, "supplierID": 1815225270, "suppliername": "Bakerspride  Co., Inc", "type": "item", "unit": "Kilo", "used": ""}, {"B1": "2", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2009133824, "itemname": "Chocolate (Vivo) (Fancy White) (Kg) (10 Pc /1)", "order": "", "ordero": "", "pending": "", "rid": 228, "saved": 0, "selected": 0, "supplierID": 1815225270, "suppliername": "Bakerspride  Co., Inc", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162475, "itemname": "Chocolate (Vivo) (Fancy Dark) (3kg) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 229, "saved": 0, "selected": 0, "supplierID": 1815225270, "suppliername": "Bakerspride  Co., Inc", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162480, "itemname": "Chocolate (Vivo) (Fancy White) (3kg) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 230, "saved": 0, "selected": 0, "supplierID": 1815225270, "suppliername": "Bakerspride  Co., Inc", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 231, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Balili Stainless Steel & Allied Metal Works", "type": "Balili Stainless Steel & Allied Metal Works", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 232, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915434613, "itemname": "Bolt (A325 ) (38mm X 2400mm) (2-2hnut & Flat Washer) (Bundle)", "order": "", "ordero": "", "pending": "12", "rid": 233, "saved": 0, "selected": 0, "supplierID": 1912082141, "suppliername": "Balili Stainless Steel & Allied Metal Works", "type": "item", "unit": "Bundle", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1915004794, "itemname": "Steel Pylon (Ground Up) (70n/a) (Pc)", "order": "", "ordero": "", "pending": "1", "rid": 234, "saved": 0, "selected": 0, "supplierID": 1912082141, "suppliername": "Balili Stainless Steel & Allied Metal Works", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 235, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Banawe Bean Curd Products", "type": "Banawe Bean Curd Products", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 236, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1917292573, "itemname": "Fish Ball (Pack)", "order": "", "ordero": "", "pending": "", "rid": 237, "saved": 0, "selected": 0, "supplierID": 1911310452, "suppliername": "Banawe Bean Curd Products", "type": "item", "unit": "pack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 238, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Bela Star Distribution System Inc.", "type": "Bela Star Distribution System Inc.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 239, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1808480337, "itemname": "Cake Flour (White King) (25kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 240, "saved": 0, "selected": 0, "supplierID": 1814575600, "suppliername": "Bela Star Distribution System Inc.", "type": "item", "unit": "Sack", "used": ""}, {"B1": "10", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161645, "itemname": "Starch (Cassava) (50kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 241, "saved": 0, "selected": 0, "supplierID": 1814575600, "suppliername": "Bela Star Distribution System Inc.", "type": "item", "unit": "Sack", "used": "3.75"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161726, "itemname": "Flour (White King) (All Purposed Flour) (25kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 242, "saved": 0, "selected": 0, "supplierID": 1814575600, "suppliername": "Bela Star Distribution System Inc.", "type": "item", "unit": "Sack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162385, "itemname": "Flour (Republic) (Bread) (25kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 243, "saved": 0, "selected": 0, "supplierID": 1814575600, "suppliername": "Bela Star Distribution System Inc.", "type": "item", "unit": "Sack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162389, "itemname": "Flour (Princess) (Cake) (25kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 244, "saved": 0, "selected": 0, "supplierID": 1814575600, "suppliername": "Bela Star Distribution System Inc.", "type": "item", "unit": "Sack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 245, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Bernard Ong ", "type": "Bernard Ong ", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 246, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814161726, "itemname": "Flour (White King) (All Purposed Flour) (25kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 247, "saved": 0, "selected": 0, "supplierID": 1808524645, "suppliername": "Bernard Ong ", "type": "item", "unit": "Sack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1817022577, "itemname": "Rice (Villasis Bago) (Dinorado) (50kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 248, "saved": 0, "selected": 0, "supplierID": 1808524645, "suppliername": "Bernard Ong ", "type": "item", "unit": "Sack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 249, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Best Choice Agri Food Industries Corp.", "type": "Best Choice Agri Food Industries Corp.", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 250, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816581421, "itemname": "Sugar (Powdered) (50kg) (Pack)", "order": "", "ordero": "", "pending": "", "rid": 251, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "pack", "used": ".25"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1914551335, "itemname": "Glucose (Peotraco) (25kg) (Pail)", "order": "", "ordero": "", "pending": "", "rid": 252, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "Pail", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1808512037, "itemname": "Sugar (Victoria) (White) (50kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 253, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "Sack", "used": ""}, {"B1": "116", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1811125634, "itemname": "Sugar (Lopez) (White) (50kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 254, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "Sack", "used": "57.16"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1812473618, "itemname": "Sugar (Busco) (White) (50kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 255, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "Sack", "used": ""}, {"B1": "76", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1812542275, "itemname": "Sugar (Caster) (25kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 256, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "Sack", "used": "5.08"}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1813492405, "itemname": "Sugar (Sonedco) (Brown) (50kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 257, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "Sack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162221, "itemname": "Flour (Erawan) (Glutinous Rice) (30kg) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 258, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "Sack", "used": ""}, {"B1": "141", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1816582619, "itemname": "Sugar (Powdered) (50kg) (22 Pack /1)", "order": "", "ordero": "", "pending": "", "rid": 259, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "Sack", "used": "6.25"}, {"B1": "85", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814162528, "itemname": "Sugar (Caster) (Sack)", "order": "", "ordero": "", "pending": "", "rid": 260, "saved": 0, "selected": 0, "supplierID": 1815094025, "suppliername": "Best Choice Agri Food Industries Corp.", "type": "item", "unit": "Sack", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 261, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Blizzardref Sales Corporation ", "type": "Blizzardref Sales Corporation ", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 262, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1814552406, "itemname": "Freezer (Haier) (Chest Type) (Sd-332) (Pc)", "order": "", "ordero": "", "pending": "1", "rid": 263, "saved": 0, "selected": 0, "supplierID": 1808284266, "suppliername": "Blizzardref Sales Corporation ", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1910042676, "itemname": "Microwave Oven (Whirlpool) (Mwx203bl) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 264, "saved": 0, "selected": 0, "supplierID": 1808284266, "suppliername": "Blizzardref Sales Corporation ", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 265, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "Bon Gre Corporation", "type": "Bon Gre Corporation", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 266, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2008575921, "itemname": "Mop Head (Green) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 267, "saved": 0, "selected": 0, "supplierID": 1913370728, "suppliername": "Bon Gre Corporation", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2009222844, "itemname": "Dust Pan (Plastic) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 268, "saved": 0, "selected": 0, "supplierID": 1913370728, "suppliername": "Bon Gre Corporation", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2014411610, "itemname": "Mop Head (Blue) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 269, "saved": 0, "selected": 0, "supplierID": 1913370728, "suppliername": "Bon Gre Corporation", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2016463447, "itemname": "Mop Head Blue #080402 (Pc)", "order": "", "ordero": "", "pending": "", "rid": 270, "saved": 0, "selected": 0, "supplierID": 1913370728, "suppliername": "Bon Gre Corporation", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 2017313648, "itemname": "Mop Head (Red) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 271, "saved": 0, "selected": 0, "supplierID": 1913370728, "suppliername": "Bon Gre Corporation", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1914485287, "itemname": "Industrial Apron (Pc)", "order": "", "ordero": "", "pending": "", "rid": 272, "saved": 0, "selected": 0, "supplierID": 1913370728, "suppliername": "Bon Gre Corporation", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1913363600, "itemname": "Face Mask (Spit Guard) (Pc)", "order": "", "ordero": "", "pending": "", "rid": 273, "saved": 0, "selected": 0, "supplierID": 1913370728, "suppliername": "Bon Gre Corporation", "type": "item", "unit": "pc", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 274, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "BONG GALANG", "type": "BONG GALANG", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": "", "itemname": "", "order": "", "ordero": "", "pending": "", "rid": 275, "saved": 0, "selected": 0, "supplierID": "", "suppliername": "header", "type": "header", "unit": "", "used": ""}, {"B1": "", "B2": "", "B3": "", "Total": "", "cold_two": "", "item_recid": 1900554882, "itemname": "Salted Egg (Pc)", "order": "", "ordero": "", "pending": "1,500", "rid": 276, "saved": 0, "selected": 0, "supplierID": 1911353019, "suppliername": "BONG GALANG", "type": "item", "unit": "pc", "used": ""}];