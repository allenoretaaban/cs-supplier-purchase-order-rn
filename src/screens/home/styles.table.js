const React = require("react-native");
const { Dimensions, Platform } = React;
const pf = Platform.OS;
const rHgt = pf === "ios" ? 38 : 42;
const rHgth = pf === "ios" ? 34 : 36;

export default {
    tblHPre: { borderColor:'#405323', borderLeftWidth:1, borderRightWidth:1, borderBottomWidth:1, minHeight: rHgth, backgroundColor:'#1c2210',
        borderRadius:1, alignItems:'center', justifyContent:'center', flex:1, paddingStart:7, paddingEnd:7, paddingTop:5, paddingBottom:5},
    tblH: { borderColor:'#405323', borderRightWidth:1, borderBottomWidth:1, minHeight: rHgth, alignItems:'center', justifyContent:'center', 
        backgroundColor:'#1c2210', alignItems:'center', width:80, paddingStart:7, paddingEnd:7},

    txtCl: { color:'gray', fontFamily:'Lato-Regular', paddingTop:0, paddingBottom:0 },
    txtC: { color:'#fad2de', fontFamily:'Lato-Regular', paddingTop:0, paddingBottom:0 },

    tblCPre: { borderColor:'#405323', borderLeftWidth:1, borderRightWidth:1, borderBottomWidth:1, height:rHgt,
        borderRadius:1, alignItems:'flex-start', justifyContent:'center', flex:1, paddingStart:7, paddingEnd:7, paddingTop:0, paddingBottom:0},
    tblC: { borderColor:'#405323', borderRightWidth:1, borderBottomWidth:1, height:rHgt, alignItems:'center', justifyContent:'center', 
        alignItems:'flex-end', width:80, paddingStart:7, paddingEnd:7, paddingTop: 0, paddingBottom:0},
    tblCc: { borderColor:'#405323', borderRightWidth:1, borderBottomWidth:1, height:rHgt, alignItems:'center', justifyContent:'center', 
        alignItems:'center', width:80, paddingStart:7, paddingEnd:7, paddingTop: 0, paddingBottom:0},

    calBtn: { height:50,width:50,justifyContent:'center',marginEnd:5,marginBottom:5,paddingTop:0,paddingBottom:0,backgroundColor:'#000',
        borderWidth:1,borderColor:'#fad2de' },
    calBtnEnt: { height:null,width:50,justifyContent:'center',marginEnd:5,marginBottom:5,paddingTop:0,paddingBottom:0,flex:1,
        backgroundColor:'rgba(0,0,0,0.7)',borderWidth:1,borderColor:'#fad2de' },
    calBtnTxt: { fontSize:35,textAlign:'center',paddingStart:0,paddingEnd:0,color:'#fff' },
    calBtnTxtT: { fontSize:20,textAlign:'center',paddingStart:0,paddingEnd:0,color:'#fff' }
}