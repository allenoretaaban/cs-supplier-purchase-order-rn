import React, { Component } from "react";
import { KeyboardAvoidingView, ImageBackground, TouchableWithoutFeedback, Dimensions, StatusBar, Keyboard, TouchableOpacity } from "react-native";
import { View, Text, Item, Input, Button, Spinner, Container, Header, Icon, Picker, CheckBox, Body } from "native-base";
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';
import Toast, { DURATION } from "react-native-easy-toast";
import Modal from "react-native-modal";
import { Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider } from 'react-native-popup-menu';

import rest from "./../../utility/requests";
import cons from "./../../constants/csvars";
import vars from "./../../theme/variables/material";
import styles from "./styles";
const { width,height } = Dimensions.get("window"); // not working on vars

import Database from "./../../db/db";
const db = new Database();

class Login extends Component {

  constructor(props) {
    console.log('constructor Login');
    super(props);
    this.inputsx = {};
    this.menuAuth = [];
    this.state = { isLoading:false, tUsername:'', tPassword:'', pwIcon:"eye-outline", pwSecure:true, isRmSelected:false, isLcSelected:true }
  }

  componentDidMount() {
    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      console.log('componentDidMount Login');
      SplashScreen.hide();

      this.setState({tUsername:'',tPassword:'',pwIcon:"eye-outline"});
      this.menuAuth = [];
      // always update users
      this.requestUsers();

      //db.deleteRset(cons.TBL.AUTH, "", null).then((data) => { });
      db.getRset(cons.TBL.AUTH, "", "", null).then((data) => {
        console.log(data);
        if (data.length > 0) {
          for (let obj of data) {
            let mVal = obj.usern+"|"+obj.passw;
            this.menuAuth.push(<MenuOption key={obj.id} value={mVal} text={obj.usern} style={{paddingTop:10,paddingBottom:10,paddingStart:10,paddingEnd:10}} />);
          }
        } 
      }).catch((err) => { });

    });
    this._removeData();
  }

  requestUsers = async() => {
    fetch(rest.SERVER + 'api/getCoPreReq?cn=' + rest.CN + '&branchid=1817114323', { headers:rest.HEADERS_PO })
    .then((response) => response.json()).then((rJson) => {
      if (rJson["staff"].length > 0) {
        let rs = rJson["staff"];
        if (rs.length > 0) {
          db.deleteRset(cons.TBL.USER, "", null).then((data) => {
            db.addUsers(rs).then((data) => { console.log("users..."); }).catch((err) => {
              this.setState({ isLoading:false });
              this.refs.eToast.show('Error on Request Users! Please inform DEVELOPER....');
            });
          });
        }
      } 
      Keyboard.dismiss();
      this.requestSuppliers();
    })
    .catch((error) => {
      this.setState({ isLoading:false });
      this.refs.eToast.show('Error on Request Users! Please inform DEVELOPER....');
      console.warn(error);
    });
  }

  requestSuppliers = async() => {
    fetch(rest.SERVER_BETA + 'api/spo/getSupplierlist?cn='+rest.CN+'&pagesize=1000&pagenumber=0&suppliername=&employee_recid=', 
    { headers:rest.HEADERS }).then((response) => response.json()).then((rs) => {   
      db.deleteRset(cons.TBL.SUPP, "", null).then((data) => { 
        db.addSuppliers(rs).then((data) => { console.log("suppliers..."); }).catch((err) => {
          this.setState({ isLoading:false });
          this.refs.eToast.show('Error on Request Suppliers! Please inform DEVELOPER....');
        });
      });
    }).catch((error) => {
      this.setState({ isLoading:false });
      this.refs.eToast.show('Error on Request Suppliers! Please inform DEVELOPER....');
      console.warn(error);
    });
  }

  _removeData = async () => {
    try {
      await AsyncStorage.removeItem('csrEmpId');
      await AsyncStorage.removeItem('csrEmail');
      await AsyncStorage.removeItem('csrEmpNo');
      await AsyncStorage.removeItem('csrName');
      return true;
    } catch(error) {
        return false;
    }
  }

  _doLogin = () => {
    Keyboard.dismiss();
    let usern = this.state.tUsername.trim();
    let passw = this.state.tPassword.trim();
    this.setState({ isLoading: true });
    setTimeout(() => {
      db.getRset(cons.TBL.USER, "WHERE (Email = ? OR empNo = ?) AND pass = ?", "", [usern,usern,passw])
      .then((data) => {
        this.setState({ isLoading: false });
        if (data.length > 0) {
          try {

            this.refs.sToast.show('Welcome! Good day ' + data[0].name + ' !!!', DURATION.LONG);

            if(this.state.isRmSelected) {
              let strUsrn =  usern.indexOf('@') > 1 ? data[0].Email : data[0].empNo;
              console.log(strUsrn);
              db.getRset(cons.TBL.AUTH, "WHERE usern = ?", "", [strUsrn])
              .then((data) => {
                if (data.length == 0) {
                  db.addAuths([{"usern":strUsrn,"passw":passw}]).then((data) => {
                    console.log("success add auth");
                  }).catch((err) => { });
                }
              }).catch((err) => { });
            }

            if(this.state.isLcSelected) this._storeData("true");
            if(!this.state.isLcSelected) this._storeData("false");

            this._storeData(data[0]);
            setTimeout(() => {
              this.setState({ isLoading: true });
              setTimeout(() => {
                this.props.navigation.navigate('Home');
                this.setState({ isLoading: false });
              }, 500);
            }, 500);
          } catch (error) {
            // Error saving data
          }
        } else {
          this.refs.eToast.show('INVALID CREDENTIALS! Please try again...', DURATION.LONG);
        }
      }).catch((err) => {
        //console.log(err);
        this.setState({ isLoading: false });
        this.refs.eToast.show('Error on Request! Please inform DEVELOPER....', DURATION.LONG);
      });
    }, 500);
  }
  
  _storeConnType = async (type) => {
    try {
      await AsyncStorage.setItem('csConnType', type);
    } catch (error) { }
  }

  _storeData = async (data) => {
    try {
      await AsyncStorage.setItem('csrEmpId', data.empId+"");
      await AsyncStorage.setItem('csrEmail', data.Email+"");
      await AsyncStorage.setItem('csrEmpNo', data.refempno+"");
      await AsyncStorage.setItem('csrName', data.name+"");
    } catch (error) {
      // Error saving data
    }
  }

  _togglePassword = () => {
    if (this.state.pwIcon == "eye-outline") {
      this.setState({ pwIcon:"eye-off-outline", pwSecure:false });
    } else {
      this.setState({ pwIcon:"eye-outline", pwSecure:true });
    }
  }

  _selectedAuth = (val) => {
    let vArr = val.split("|");
    this.setState({ tUsername:vArr[0],tPassword:vArr[1] });
  }

  render() {
    return (
      <Container>
        <KeyboardAvoidingView style={{ flex:1 }} enabled={true} behavior={Platform.OS==='ios'?'padding':null} >
          <StatusBar barStyle="light-content" />
          <ImageBackground source={cons.BCKGRND} style={cons.CNTR_STYL}>
            <TouchableWithoutFeedback style={{flex:1}} onPress={() => { Keyboard.dismiss(); }} >
              <View style={cons.OVRLY}>
                <View style={{ justifyContent:'center',flex:1 }}>
                  <Text style={styles.logoTitle}>Supplier Purchase Order</Text>
                  <View style={{ marginTop:50,width:'40%',alignSelf:'center' }}>
                    <Item regular style={{ paddingHorizontal:7, marginBottom:10, borderRadius:3 }} >
                      <Input border placeholder='Username' returnKeyType={'next'}
                        blurOnSubmit = {false}
                        value={this.state.tUsername}
                        onChangeText={(value) => { this.setState({ tUsername:value }) }}
                        onSubmitEditing={() => { this.inputsx['tPassword']._root.focus() }}
                        style={{paddingBottom:5}}
                      />  
                        <Menu onSelect={(value) => { this._selectedAuth(value); }}>
                          <MenuTrigger>
                            <Icon name="chevron-down-outline" style={{color:"gold"}}/>
                          </MenuTrigger>
                          <MenuOptions>
                            {this.menuAuth}
                          </MenuOptions>
                        </Menu>
                    </Item>
                    <Item regular style={{ paddingHorizontal:7, borderRadius:3 }} >
                      <Input secureTextEntry={this.state.pwSecure} placeholder='Password' returnKeyType={'done'}
                        value={this.state.tPassword}
                        onChangeText={(value) => { this.setState({ tPassword:value }) }}
                        ref={(input) => { this.inputsx['tPassword'] = input; }}
                        style={{paddingBottom:5}}
                      />                        
                      <TouchableOpacity onPress={() => { this._togglePassword(); }}>
                        <Icon name={this.state.pwIcon} style={{color:"gold"}}/>
                      </TouchableOpacity>
                    </Item>
                    <View style={{  marginTop:15,marginBottom:30,flexDirection:'row' }}>
                      <CheckBox style={{ borderColor:vars.brandPrimary, borderRadius:3 }} checked={this.state.isRmSelected} 
                        onPress={() => { this.setState({isRmSelected:!this.state.isRmSelected}); }} />
                      <Text style={{ color:vars.brandPrimary,marginStart:17 }}>Remember me?</Text>
                    </View>
                    <Button block style={{ borderColor:'transparent' }} onPress={() => { this._doLogin(); }} >
                      <Text style={{ height:null,fontSize:17 }}>Log-In</Text>
                    </Button>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </ImageBackground>
          <Toast ref="eToast" style={{backgroundColor:vars.brandDanger,paddingHorizontal:20,paddingVertical:15}} 
            textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
          <Toast ref="iToast" style={{backgroundColor:vars.brandInfo,paddingHorizontal:20,paddingVertical:15}} 
            textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
          <Toast ref="sToast" style={{backgroundColor:vars.brandSuccess,paddingHorizontal:20,paddingVertical:15}} 
            textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
          <Toast ref="wToast" style={{backgroundColor:vars.brandWarning,paddingHorizontal:20,paddingVertical:15}} 
            textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
          <Modal backdropColor='#000' backdropOpacity={0.4} isVisible={this.state.isLoading}>
            <View style={{ flex: 1 }}>
              <Spinner style={{ flex:1 }} />
            </View>
          </Modal>

          <View style={{  marginTop:30,marginBottom:30,flexDirection:'row',position:'absolute' }}>
            <CheckBox style={{ borderColor:vars.brandPrimary, borderRadius:3 }} checked={this.state.isLcSelected} 
              onPress={() => { this.setState({isLcSelected:!this.state.isLcSelected}); }} />
            <Text style={{ color:vars.brandPrimary,marginStart:17 }}>Enable Local Connection?</Text>
          </View>
        </KeyboardAvoidingView>
      </Container>
    );
  }


}

export default Login;