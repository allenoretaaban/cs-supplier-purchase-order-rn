import React, { Component } from 'react';
import { ImageBackground, StatusBar } from "react-native";
import { Container, View, Spinner } from "native-base";
import cons from "./../../constants/csvars";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast, { DURATION } from "react-native-easy-toast";
import SplashScreen from 'react-native-splash-screen';

import vars from "./../../theme/variables/material";
import rest from "./../../utility/requests";

import Database from "./../../db/db";
const db = new Database();
var itmCtr = 0;

class LoadingScreen extends Component {

  constructor(props) {
    console.log('constructor Loader');
    SplashScreen.hide();
    super(props);
    this.state = { isLoading:false };
    this.sIds = [];

    /*setTimeout(() => {
      //db.deleteRset(cons.TBL.USER, "", null).then((data) => { });

      db.getRset(cons.TBL.USER, "", "", null).then((data) => {
        if (data.length == 0) {
          this.setState({ isLoading:true });
          this.refs.iToast.show('FETCHING REQUIRED DATA, Please wait....');
          this.requestUsers();
        } else {
          this._bootstrapAsync();
        }
      }).catch((err) => {
        this.refs.eToast.show('Error on Request! Please inform DEVELOPER....', DURATION.LONG);
        console.log(err);
      });
    }, 500); */

    this._bootstrapAsync();
  }

  /*componentDidMount() {
    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      console.log('componentDidMount Loader');    
      

    });
  }*/

  requestUsers = async() => {
    fetch(rest.SERVER + 'api/getCoPreReq?cn=' + rest.CN + '&branchid=1817114323', 
    { headers:rest.HEADERS_PO }).then((response) => response.json()).then((rJson) => {
      if (rJson["staff"].length > 0) {
        let rs = rJson["staff"];
        if (rs.length > 0) {
          db.deleteRset(cons.TBL.USER, "", null).then((data) => {
            db.addUsers(rs).then((data) => {
              console.log("users...");
              db.deleteRset(cons.TBL.SUPP, "", null).then((data) => { });
              this.requestSuppliers();
            }).catch((err) => {
              this.refs.eToast.show('Error on Request! Please inform DEVELOPER....', DURATION.LONG);
              this.setState({ isLoading:false });
            });
          });
        } else {
          this.refs.eToast.show('Error on Request! Please inform DEVELOPER....', DURATION.LONG);
          this.setState({ isLoading:false });
        }
      } else {
        this.refs.eToast.show('Error on Request! Please inform DEVELOPER....', DURATION.LONG);
        this.setState({ isLoading:false });
      }
    }).catch((error) => {
      this.setState({ isLoading:false });
      alert("Error on Request! Please inform DEVELOPER....");
      console.warn(error);
    });
  }

  requestSuppliers = async() => {
    fetch(rest.SERVER_BETA + 'api/spo/getSupplierlist?cn='+rest.CN+'&pagesize=1000&pagenumber=0&suppliername=&employee_recid=', 
    { headers:rest.HEADERS }).then((response) => response.json()).then((rs) => {   
      db.deleteRset(cons.TBL.SUPP, "", null).then((data) => { 
        db.addSuppliers(rs).then((data) => {
          console.log("suppliers...");
          /*db.getRset(cons.TBL.SUPP, "", "", null).then((data) => {
            db.deleteRset(cons.TBL.SUPPITMS, "", null).then((data) => { });
            for (let obj of data) {  
              //this.sIds.push(obj.supplierID); 
              this.requestSuppliersItems(obj.supplierID);
            }
            itmCtr = 0;
            //this.requestSuppliersItems(this.sIds[itmCtr]);
          }).catch((err) => { }); */
          setTimeout(() => {
            this.setState({ isLoading:false });
            this.refs.sToast.show('USER RECORDS successfully UPDATED!');
            setTimeout(() => {
              this._bootstrapAsync();
            }, 1000);
          }, 1000);

        }).catch((err) => {
          this.refs.eToast.show('Error on Request! Please inform DEVELOPER....', DURATION.LONG);
          this.setState({ isLoading:false });
        });
      });
    }).catch((error) => {
      this.setState({ isLoading:false });
      alert("Error on Request! Please inform DEVELOPER....");
      console.warn(error);
    });
  }

  requestSuppliersItems = async(id) => {
    console.log(id);
    fetch(rest.SERVER_BETA + 'api/spo/getSupplierItems?cn=backoffice&supplier_recid='+parseInt(id), 
    { headers:rest.HEADERS }).then((response) => response.json()).then((rs) => {  
      console.log(rs[0].suppliername);
      /*if (itmCtr < this.sIds.length) {
        itmCtr++;
        this.requestSuppliersItems(this.sIds[itmCtr]);
      }*/
    }).catch((error) => {
      this.setState({ isLoading:false });
      alert("Error on Request! Please inform DEVELOPER....");
      console.warn(error);
    });
  }

  _bootstrapAsync = async () => {
    try {
      const vEmpId = await AsyncStorage.getItem('csrEmpId');
      const vEmail = await AsyncStorage.getItem('csrEmail');
      if (vEmpId !== null || vEmail !== null) {
        this.props.navigation.navigate('Home');
      } else {
        this.props.navigation.navigate('Login');
      }
    } catch (error) {
      this.props.navigation.navigate('Login');
    }
  };

  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <ImageBackground source={cons.BCKGRND} style={cons.CNTR_STYL}>
          <View style={cons.OVRLY}>
            <Spinner color='orange' style={{ flex:1, height:null }}/>
          </View>
        </ImageBackground>
        <Toast ref="eToast" style={{backgroundColor:vars.brandDanger,paddingHorizontal:20,paddingVertical:15}} 
          textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
        <Toast ref="iToast" style={{backgroundColor:vars.brandInfo,paddingHorizontal:20,paddingVertical:15}} 
          textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
        <Toast ref="sToast" style={{backgroundColor:vars.brandSuccess,paddingHorizontal:20,paddingVertical:15}} 
          textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
        <Toast ref="wToast" style={{backgroundColor:vars.brandWarning,paddingHorizontal:20,paddingVertical:15}} 
          textStyle={{color:'white',fontSize:18}} opacity={cons.TOAST_OPACITY} position='bottom' fadeOutDuration={700}/>
      </Container>
    );
  }
}

export default LoadingScreen;
