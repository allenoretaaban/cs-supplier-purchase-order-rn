export default {

    getCurrentDate() {
      var d = new Date(), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [year, month, day].join('-');
    },

    getCurrentDateTime() {
      var d = new Date(), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [year, month, day].join('-')+' '+[d.getHours(), d.getMinutes(), d.getSeconds()].join(':');
    },

    getCurrentDateTimeDisplay() {
      var d = new Date(), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      var strAmPm = d.getHours()>=12 ? "PM" : "AM";
      return [month, day, year].join('/')+' '+[d.getHours()>12 ? d.getHours()-12 : d.getHours(), d.getMinutes().toString().length!=1 ? d.getMinutes() : "0"+d.getMinutes(), 
        d.getSeconds().toString().length!=1 ? d.getSeconds() : "0"+d.getSeconds()].join(':') + " " + strAmPm;
    },

    formatStrToInt(vStr) {
      if (vStr == null) { 
        vStr = "0"; 
      } else if (vStr.toString().trim() == "") { 
        vStr = "0"; 
      }
      var vInt =  parseInt(vStr.toString().trim().replace(/,/g,""));
      return vInt;
    },
    nWithCommas(vStr) {
      return vStr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },

    getPODate() {
      var d = new Date(), 
        month = ''+(d.getMonth() + 1), 
        day = ''+d.getDate().toString(), 
        year = d.getFullYear().toString().substring(0,2),
        hours = d.getHours()>12 ? d.getHours()-12 : d.getHours(),
        minutes = d.getMinutes().toString().length!=1 ? ''+d.getMinutes() : '0'+d.getMinutes()
        seconds = d.getSeconds().toString().length!=1 ? ''+d.getSeconds() : '0'+d.getSeconds();
        msec = d.getMilliseconds();
      if (month.length < 2) month = '0'+month;
      if (day.length < 2) day = '0'+day;
      return [year, month, day, hours.toString().length!=1 ? ''+hours : '0'+hours, minutes, seconds, msec.toString().substring(0,2)].join('');
    },    
    
    getPODateItem() {
      var d = new Date(), 
        month = ''+(d.getMonth() + 1), 
        day = ''+d.getDate().toString(), 
        year = d.getFullYear();
      if (month.length < 2) month = '0'+month;
      if (day.length < 2) day = '0'+day;
      return [month, day, year].join('');
    },
  
  };