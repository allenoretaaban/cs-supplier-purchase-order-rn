import React, { Component } from "react-native";
import SQLite from "react-native-sqlite-storage";

// SQLite.DEBUG(true);
SQLite.enablePromise(true);

const db_name = "CSCommiPO7b.db";
const db_version = "1.0";
const db_displayname = "SQLite React Offline Database";
const db_size = -1;

export default class Database { 
  initDB() {
	let db;
	return new Promise((resolve) => {
	  SQLite.echoTest()
      .then(() => {
	    console.log("integrity check passed ... opening database ...");
        var opts = Platform.OS === "ios" ? {name:db_name, location:'Documents' } : {name:'main', createFromLocation:db_name }

	    SQLite.openDatabase( db_name, db_version, db_displayname, db_size
        ).then(DB => {

          DB.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS users (' +
              'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
              'empId TEXT NULL, ' +
              'refempno TEXT NULL, ' +
              'empNo TEXT NULL, ' +
              'Email TEXT NULL, ' +
              'name TEXT NULL, ' +
              'Branch TEXT NULL, ' +
              'Jobtitle TEXT NULL, ' +
              'pass TEXT NULL, ' +
              'active TEXT NULL, ' +
              'ismobileadmin TEXT NULL)'
            );
          }).then(() => { //console.log("Table users created successfully"); 
          }).catch(error => { console.error(error); });

          DB.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS auths (' +
              'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
              'usern TEXT NULL, ' +
              'passw TEXT NULL)'
            );
          }).then(() => { //console.log("Table auths created successfully"); 
          }).catch(error => { console.error(error); });

          DB.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS suppliers (' +
              'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
              'recid TEXT NULL, ' +
              'supplierID TEXT NULL, ' +
              'suppliername TEXT NULL, ' +
              'deleted TEXT NULL, ' +
              'isallowedPOApproval TEXT NULL)'
            );
          }).then(() => { //console.log("Table suppliers created successfully"); 
          }).catch(error => { console.error(error); });

          DB.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS items (' +
              'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
              'item_recid TEXT NULL, ' +
              'supplierID TEXT NULL, ' +
              'itemname TEXT NULL, ' +
              'unit TEXT NULL, ' +
              'B1 TEXT NULL, ' +
              'B2 TEXT NULL, ' +
              'B3 TEXT NULL, ' +
              'cold_two TEXT NULL, ' +
              'Total TEXT NULL, ' +
              'used TEXT NULL, ' +
              'pending TEXT NULL, ' +
              'orderv TEXT NULL)'
            );
          }).then(() => { //console.log("Table users created successfully"); 
          }).catch(error => { console.error(error); });

          DB.transaction((tx) => {
            tx.executeSql('CREATE TABLE IF NOT EXISTS porefxs (' +
              'id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
              'supplierID TEXT NULL, ' +
              'porecid TEXT NULL)'
            );
          }).then(() => { //console.log("Table porefxs created successfully"); 
          }).catch(error => { console.error(error); });

          DB.close();
          resolve(DB);
        }).catch(error => {
          console.error(error);
        });
      }).catch(error => {
        console.error(error);
      });
	});
  };
    
  closeDatabase(DB) {
    if (DB) {
      DB.close().then(status => { console.log("DB CLOSED"); }).catch(error => { console.error(error); });
    } else {
      console.error("DB was not OPENED");
    }
  };

  deleteRset(table, filter, orderby, val) { 
    return new Promise((resolve) => {
      const rset = [];	    
      SQLite.openDatabase( db_name, db_version, db_displayname, db_size
      ).then(DB => {
        DB.transaction((tx) => {
          tx.executeSql("DELETE FROM " + table + " " + filter, val).then(([tx, results]) => {
            resolve(results);
          }); 
        }).catch(error => { console.error(error); this.closeDatabase(DB); });
      }).catch(error => {
        console.warn(error);
        this.closeDatabase(DB);
      });
    });
  };

  getRset(table, filter, orderby, val) { 
    return new Promise((resolve) => {
      const rset = [];	    
      SQLite.openDatabase( db_name, db_version, db_displayname, db_size
      ).then(DB => {
        DB.transaction((tx) => {
          let sqlStr = "SELECT * FROM " + table + " " + filter + " " + orderby; //console.log(sqlStr);
          tx.executeSql(sqlStr, val).then(([tx, results]) => {
            if (results.rows.length > 0) {
              for (let i = 0; i < results.rows.length; i++) {
                rset.push(results.rows.item(i));
              }
            }
            resolve(rset);
          });
        }).catch(error => { console.error(error); this.closeDatabase(DB); });
      }).catch(error => {
        console.warn(error);
        this.closeDatabase(DB);
      });
    });
  };

  addUsers(uiRx) { 
    return new Promise((resolve) => {
      const rset = [];	    
      SQLite.openDatabase( db_name, db_version, db_displayname, db_size
      ).then(DB => {
        DB.transaction((tx) => {
          for(let i = 0; i < uiRx.length; i++) {
            let xRs = [
              null, uiRx[i].empId, uiRx[i].refempno, uiRx[i].empNo, uiRx[i].Email, uiRx[i].name,
              uiRx[i].Branch, uiRx[i].Jobtitle, uiRx[i].pass, uiRx[i].active, uiRx[i].ismobileadmin
            ];
            tx.executeSql('INSERT INTO users VALUES (?,?,?,?,?,?,?,?,?,?,?)', xRs).then(([tx, results]) => { /*console.log(results);*/ });
          }
        }).then(() => {
          resolve("Insert users success...");
        }).catch(error => { console.error(error); this.closeDatabase(DB); });
      }).catch(error => {
        console.warn(error);
        this.closeDatabase(DB);
      });
    });
  };

  addSuppliers(uiRx) { 
    return new Promise((resolve) => {
      const rset = [];	    
      SQLite.openDatabase( db_name, db_version, db_displayname, db_size
      ).then(DB => {
        DB.transaction((tx) => {
          for(let i = 0; i < uiRx.length; i++) {
            let xRs = [
              null, uiRx[i].recid, uiRx[i].supplierID, uiRx[i].suppliername, uiRx[i].deleted, uiRx[i].isallowedPOApproval
            ];
            tx.executeSql('INSERT INTO suppliers VALUES (?,?,?,?,?,?)', xRs).then(([tx, results]) => { /*console.log(results);*/ });
          }
        }).then(() => {
          resolve("Insert suppliers success...");
        }).catch(error => { console.error(error); this.closeDatabase(DB); });
      }).catch(error => {
        console.warn(error);
        this.closeDatabase(DB);
      });
    });
  };

  addItems(uiRx) { 
    return new Promise((resolve) => {
      const rset = [];	    
      SQLite.openDatabase( db_name, db_version, db_displayname, db_size
      ).then(DB => {
        DB.transaction((tx) => {
          for(let i = 0; i < uiRx.length; i++) {
            let xRs = [
              null, uiRx[i].item_recid, uiRx[i].supplierID, uiRx[i].itemname, uiRx[i].unit, uiRx[i].B1, uiRx[i].B2, uiRx[i].B2,
              uiRx[i].cold_two, uiRx[i].Total, uiRx[i].used, uiRx[i].pending, uiRx[i].order
            ];
            tx.executeSql('INSERT INTO items VALUES (?,?,?,?,?,?)', xRs).then(([tx, results]) => { /*console.log(results);*/ });
          }
        }).then(() => {
          resolve("Insert supplier items success...");
        }).catch(error => { console.error(error); this.closeDatabase(DB); });
      }).catch(error => {
        console.warn(error);
        this.closeDatabase(DB);
      });
    });
  };

  addAuths(uiRx) { 
    return new Promise((resolve) => {
      const rset = [];	    
      SQLite.openDatabase( db_name, db_version, db_displayname, db_size
      ).then(DB => {
        DB.transaction((tx) => {
          for(let i = 0; i < uiRx.length; i++) {
            let xRs = [
              null, uiRx[i].usern, uiRx[i].passw
            ];
            tx.executeSql('INSERT INTO auths VALUES (?,?,?)', xRs).then(([tx, results]) => { /*console.log(results);*/ });
          }
        }).then(() => {
          resolve("Insert auth success...");
        }).catch(error => { console.error(error); this.closeDatabase(DB); });
      }).catch(error => {
        console.warn(error);
        this.closeDatabase(DB);
      });
    });
  };

  addPoRefs(uiRx) { 
    return new Promise((resolve) => {
      const rset = [];	    
      SQLite.openDatabase( db_name, db_version, db_displayname, db_size
      ).then(DB => {
        DB.transaction((tx) => {
          for(let i = 0; i < uiRx.length; i++) {
            let xRs = [
              null, uiRx[i].supplierID, uiRx[i].porecid
            ];
            tx.executeSql('INSERT INTO porefxs VALUES (?,?,?)', xRs).then(([tx, results]) => { /*console.error(results);*/ });
          }
        }).then((res) => {
          resolve("Insert porefs success...");
        }).catch(error => { console.error(error); this.closeDatabase(DB); });
      }).catch(error => {
        console.warn(error);
        this.closeDatabase(DB);
      });
    });
  };

}