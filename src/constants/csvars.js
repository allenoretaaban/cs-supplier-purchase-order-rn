import React, { Component } from "react";
import { Col, Row, Grid } from "react-native-easy-grid";
import { View } from "native-base";
import { ActivityIndicator, TouchableWithoutFeedback } from "react-native";
import { Dimensions, Platform, StyleSheet } from "react-native";
import vars from "./../theme/variables/material";

const mainBg = require("../../assets/bg_e.jpg");
const drawerBg = require("../../assets/swiper_bg.png");
const drawerLogo = require("../../assets/logo.png");

export default {

  D_HGT: Dimensions.get("window").height,
  D_WDT: Dimensions.get("window").width,
  M_HGT: Dimensions.get("window").height-500,

  OVRLY: { flex:1, backgroundColor:'rgba(0,0,0, 0.88)' },
  OVRLYA: { flex:1, backgroundColor:'rgba(0,0,0)' },
  BCKGRND: mainBg,
  DRWRBG: drawerBg,
  DRWRLG: drawerLogo,
  CNTR_STYL: { flex: 1, width: null, height: null },
  ICBG: "#fad2de",
  TOAST_OPACITY: 0.9,

  STYLE_MTRIG: { width:40, alignItems:'center', justifyContent:'center', marginStart:0 },
  STYLE_MOPT: { flex:1, height:46, borderBottomWidth:1, borderColor:vars.brandSuccess, flexDirection:'row', alignItems:'center', backgroundColor:'rgba(0,0,0,0)' },
  STYLE_MOPT_I: { marginLeft:5, height:null, textAlignVertical:'center', color:'gold', fontSize:23 },
  STYLE_MOPT_T: { flex:1, height:null, textAlignVertical:'center', fontSize:Platform.OS==="ios"?14:14, color:'#fad2de', marginLeft:7 },

  TBL: { USER:"users", SUPP:"suppliers", SUPPITMS:"items", AUTH:"auths", POREF:"porefxs" },
  TableLoader: <Row><View style={{flex:1,padding:15}}><ActivityIndicator/></View></Row>,

  LOREM: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

};
